<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class updateUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:updateUsers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command para atualizar informações da tabela user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
     public function handle()
     {

       $sql ="SELECT
       *
       FROM morana2.user;
       ";

       $users_prod = DB::connection('mysql2')->select($sql);

       DB::table('backlog.user')->truncate();

       foreach ($users_prod as $user) {

         $sql = "INSERT INTO backlog.user
         SET
         user_id = '".$user->user_id."',
         user_group_id = '".$user->user_group_id."',
         username = '".$user->username."',
         password = '".$user->password."',
         salt = '".$user->salt."',
         firstname = '".$user->firstname."',
         lastname = '".$user->lastname."',
         email = '".$user->email."',
         code = '".$user->code."',
         ip = '".$user->ip."',
         status = '".$user->status."',
         date_added = '".$user->date_added."',
         date_password_changed = '".$user->date_password_changed."',
         shopping_limit = '".$user->shopping_limit."'  ";

         $query = DB::insert($sql);

         if($query <=0 || $query == '' ){
           print_R("Algum erro ocorreu ao inserir user: '".$user->user_id."'\n");die;
         }else{
           print_r("Inserido user:'".$user->user_id."'\n");
         }
       }



     }
}
