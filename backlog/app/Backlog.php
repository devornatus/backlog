<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;



class Backlog extends Authenticatable
{
  use Notifiable;

  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'name', 'email', 'password',
  ];

  /**
  * The attributes that should be hidden for arrays.
  *
  * @var array
  */
  protected $hidden = [
    'password', 'remember_token',
  ];

  static function getBacklogsByUserId($data)
  {

    $sql = "SELECT
    s.*,
    st.descricao as descricao_status,
    sist.descricao as nome_plataform
    FROM solicitacao as s
    JOIN status as st ON st.status_id = s.status_id
    JOIN sistemas as sist ON sist.sistema_id = s.sistema_id
    WHERE s.user_id = '".$data['user_id']."' ";

    if($data['filter_backlog'] != '')
    {
      $sql.= " AND s.descricao LIKE '%".$data['filter_backlog']."%' ";
    }

    if($data['filter_system'] > 0)
    {
      $sql.= " AND s.sistema_id = '".$data['filter_system']."' ";
    }

    $sql.= " ORDER BY s.solicitacao_id DESC ";

    return (array)DB::select($sql);

  }

  static function getSystems()
  {

    $sql = "SELECT
    *
    FROM sistemas
    ORDER BY descricao ASC";

    return (array)DB::select($sql);

  }

  static function createSolicitation($data)
  {

    $sql = "INSERT INTO solicitacao
    SET
    descricao = '".$data['descricao']."',
    finalidade = '".$data['finalidade']."',
    user_id = '".$data['user_id']."',
    sistema_id = '".$data['sistema_id']."'
    ";

    DB::insert($sql);
    $id = DB::getPdo()->lastInsertId();
    return $id;

  }

  static function logStatus($data)
  {

    $sql = "INSERT INTO log_status
    SET
    status_id = '".$data['status_id']."',
    solicitacao_id = '".$data['solicitacao_id']."',
    date_added = current_date
    ";

    return DB::insert($sql);

  }

  static function getSolicitationById($data)
  {

    $sql = "SELECT
    s.*,
    st.descricao as descricao_status,
    sit.descricao as descricao_sistema
    FROM solicitacao as s
    JOIN status as st ON st.status_id = s.status_id
    JOIN sistemas as sit ON sit.sistema_id = s.sistema_id
    WHERE s.solicitacao_id = '".$data['solicitacao_id']."' ";

    return (array)DB::select($sql);

  }

  static function getBacklogs($data)
  {

    $sql = "SELECT
    s.*,
    st.descricao as descricao_status,
    sist.descricao as nome_plataform,
    (select username from user where user_id = s.user_dev_id) as dev_name,
    u.username,
    st.color
    FROM solicitacao as s
    JOIN status as st ON st.status_id = s.status_id
    JOIN sistemas as sist ON sist.sistema_id = s.sistema_id
    JOIN user as u ON u.user_id = s.user_id
    LEFT JOIN devs as d ON d.user_id = s.user_dev_id
    WHERE 1 ";

    if($data['filter_backlog'] != '')
    {
      $sql.= " AND (UPPER(s.descricao) LIKE UPPER('%".$data['filter_backlog']."%')
      OR UPPER(u.email) LIKE UPPER('%".$data['filter_backlog']."%')
      OR UPPER(u.username) LIKE UPPER('%".$data['filter_backlog']."%') )";
    }

    if($data['filter_system'] > 0)
    {
      $sql.= " AND s.sistema_id = '".$data['filter_system']."' ";
    }

    if($data['filter_status'] > 0 && $data['filter_status'] != 99)
    {
      $sql.= " AND s.status_id = '".$data['filter_status']."' ";
    }else
    {
      $sql.= " AND s.status_id IN (1,2,3) ";
    }

    if($data['filter_dev'] > 0)
    {
      $sql.= " AND s.user_dev_id = '".$data['filter_dev']."' ";
    }

    $sql.= " ORDER BY s.solicitacao_id DESC ";

    return (array)DB::select($sql);

  }

  static function getStatus()
  {

    $sql = "SELECT
    *
    FROM status ";

    return (array)DB::select($sql);

  }

  static function getTotalBacklogs()
  {

    $sql = "SELECT
    concat(st.descricao,' (', count(1),')') as status_name,
    count(1) as value,
    st.color as color
    FROM solicitacao as s
    JOIN status st ON st.status_id = s.status_id
    group by s.status_id";

    return (array)DB::select($sql);

  }

  static function getMessagesSolicitationById($data)
  {

    $sql = "SELECT
    s.*,
    u.username
    FROM solicitacao_mensagem as s
    JOIN user as u ON u.user_id = s.user_id
    WHERE s.solicitacao_id = '".$data['solicitacao_id']."'
    AND s.active = 1
    ORDER BY s.solicitacao_mensagem_id ASC ";

    return (array)DB::select($sql);

  }


  static function getDevs()
  {

    $sql = "SELECT
    u.user_id,
    u.username
    FROM devs as d
    JOIN user as u ON u.user_id = d.user_id
    ORDER BY u.username ASC";

    return (array)DB::select($sql);

  }

  static function startSolicitation($data)
  {

    $sql = "UPDATE solicitacao
    SET
    user_dev_id = '".$data['dev_id']."',
    data_entrega = '".$data['data_entrega']."',
    status_id = '".$data['status_id']."'
    WHERE
    solicitacao_id = '".$data['solicitacao_id']."'
    AND active = 1

    ";

    return (array)DB::update($sql);

  }

  static function insertMessageBySolicitationId($data)
  {

    $sql = "INSERT INTO solicitacao_mensagem
    SET
    solicitacao_id = '".$data['solicitacao_id']."',
    user_id = '".$data['user_id']."',
    texto = '".$data['descricao']."'
    ";

    return DB::insert($sql);

  }

  static function getSqlSolicitationById($data)
  {

    $sql = "SELECT
    s.*,
    u.username
    FROM solicitacao_sql as s
    JOIN user as u ON u.user_id = s.user_id
    WHERE s.solicitacao_id = '".$data['solicitacao_id']."'
    AND s.active = 1
    ORDER BY s.solicitacao_sql_id ASC ";

    return (array)DB::select($sql);

  }

  static function insertSqlBySolicitationId($data)
  {

    $sql = "INSERT INTO solicitacao_sql
    SET
    solicitacao_id = '".$data['solicitacao_id']."',
    user_id = '".$data['user_id']."',
    texto = '".$data['descricao_sql']."'
    ";

    return DB::insert($sql);

  }

  static function removeSqlBySolicitationId($data)
  {

    $sql = "UPDATE solicitacao_sql
    SET
    active = '0'
    WHERE
    solicitacao_id = '".$data['solicitacao_id']."'
    AND solicitacao_sql_id = '".$data['solicitacao_sql_id']."'
    AND active = 1

    ";

    return (array)DB::update($sql);

  }

  static function startDevelopBySolicitationId($data)
  {

    $sql = "UPDATE solicitacao
    SET
    branch = '".$data['branch']."',
    user_dev_id = '".$data['dev_id']."',
    status_id = '".$data['status_id']."',
    data_desenvolvimento = current_date
    WHERE
    solicitacao_id = '".$data['solicitacao_id']."'
    AND active = 1

    ";

    return (array)DB::update($sql);

  }


  static function homologationBySolicitationId($data)
  {

    $sql = "UPDATE solicitacao
    SET
    status_id = '".$data['status_id']."'
    WHERE
    solicitacao_id = '".$data['solicitacao_id']."'
    AND active = 1 ";

    return (array)DB::update($sql);

  }

  static function reproveHomologationBySolicitationId($data)
  {

    $sql = "UPDATE solicitacao
    SET
    status_id = '".$data['status_id']."'
    WHERE
    solicitacao_id = '".$data['solicitacao_id']."'
    AND active = 1 ";

    return (array)DB::update($sql);

  }

  static function aproveHomologationBySolicitationId($data)
  {

    $sql = "UPDATE solicitacao
    SET
    status_id = '".$data['status_id']."'
    WHERE
    solicitacao_id = '".$data['solicitacao_id']."'
    AND active = 1 ";

    return (array)DB::update($sql);

  }

  static function aproveBySolicitationId($data)
  {

    $sql = "UPDATE solicitacao
    SET
    status_id = '".$data['status_id']."',
    data_producao = current_date
    WHERE
    solicitacao_id = '".$data['solicitacao_id']."'
    AND active = 1 ";

    return (array)DB::update($sql);

  }

  static function finishBySolicitationId($data)
  {

    $sql = "UPDATE solicitacao
    SET
    status_id = '".$data['status_id']."',
    justificativa = '".$data['justificativa']."',
    user_dev_id = '".$data['user_id']."'
    WHERE
    solicitacao_id = '".$data['solicitacao_id']."'
    AND active = 1 ";

    return (array)DB::update($sql);

  }


  static function getUsers()
  {

    $sql = "SELECT
    user_id,
    username
    from user
    where status = 1
    ORDER BY username ASC ";

    return (array)DB::select($sql);

  }


  static function createSolicitationDev($data)
  {

    $sql = "INSERT INTO solicitacao
    SET
    descricao = '".$data['descricao']."',
    finalidade = '".$data['finalidade']."',
    user_id = '".$data['user_id']."',
    sistema_id = '".$data['sistema_id']."'
    ";

    DB::insert($sql);
    $id = DB::getPdo()->lastInsertId();
    return $id;

  }

  static function searchDecription($data)
  {

    $sql = "SELECT
    *
    from solicitacao
    where descricao like '%".$data['filter_description']."%'
    AND active = 1
    ORDER BY solicitacao_id DESC ";

    return (array)DB::select($sql);

  }

  static function searchQuery($data)
  {

    $sql = "SELECT
    *
    from solicitacao_sql
    where texto like '%".$data['filter_query']."%'
    AND active = 1
    ORDER BY solicitacao_sql_id DESC ";

    return (array)DB::select($sql);

  }

  static function savePhoto($data)
  {

    $sql = "INSERT INTO image SET
    name = '".$data['image']['name']."',
    size = '".$data['image']['size']."',
    extension = '".$data['image']['extension']."',
    local_file = '".$data['image']['local_file']."',
    tmp_name = '".addslashes($data['image']['tmp_name'])."',
    user_id = '".$data['user_id']."',
    date_added = current_timestamp()
    ;";

    DB::insert($sql);
    $id = DB::getPdo()->lastInsertId();
    return $id;

  }

  static function getImageById($image_id)
  {

    $sql = "SELECT
    *
    FROM image
    where image_id = '".$image_id."' ;
    ;";

    return (array)DB::select($sql);

  }

  static function saveImagebySolicitationId($data)
  {

    if(count($data['images']) > 0)
    {
      foreach ($data['images'] as $image)
      {
        $sql = "UPDATE image SET
        solicitacao_id = '".$data['solicitacao_id']."'
        WHERE
        image_id = '".$image."'
        ;";
        DB::update($sql);
      }
      return true;
    }
  }

  static function getImagesBySolicitationId($data)
  {

    $sql = "SELECT
    i.*,
    u.username,
    u.user_id
    FROM image as i
    JOIN user as u ON u.user_id = i.user_id
    where i.solicitacao_id = '".$data['solicitacao_id']."'
    AND i.ativo = 1
    ORDER BY i.image_id DESC ";

    return (array)DB::select($sql);

  }

  static function savePhotoWithSolicitationId($data)
  {

    $sql = "INSERT INTO image SET
    name = '".$data['image']['name']."',
    size = '".$data['image']['size']."',
    extension = '".$data['image']['extension']."',
    local_file = '".$data['image']['local_file']."',
    tmp_name = '".addslashes($data['image']['tmp_name'])."',
    user_id = '".$data['user_id']."',
    date_added = current_timestamp(),
    solicitacao_id = '".$data['solicitacao_id']."'
    ;";

    DB::insert($sql);
    $id = DB::getPdo()->lastInsertId();
    return $id;

  }

  static function getImagesBySolicitationIdAndUserId($data)
  {

    $sql = "SELECT
    i.*,
    u.username,
    u.user_id
    FROM image as i
    JOIN user as u ON u.user_id = i.user_id
    where i.solicitacao_id = '".$data['solicitacao_id']."'
    AND i.user_id = '".$data['user_id']."'
    AND i.ativo = 1
    ORDER BY i.image_id DESC ";

    return (array)DB::select($sql);

  }


  static function linkImagebySolicitationId($data)
  {

    $sql = "UPDATE image SET
    solicitacao_id = '".$data['solicitacao_id']."'
    WHERE
    image_id = '".$data['image_id']."' ";

    return (array)DB::update($sql);

  }

  static function removeImage($data)
  {

    $sql = "UPDATE image SET
    ativo = 0
    WHERE
    image_id = '".$data['image_id']."'
    AND solicitacao_id = '".$data['solicitacao_id']."' ";

    return (array)DB::update($sql);

  }




}
