<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;



class User extends Authenticatable
{
  use Notifiable;

  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'name', 'username', 'password',
  ];

  /**
  * The attributes that should be hidden for arrays.
  *
  * @var array
  */
  protected $hidden = [
    'password', 'remember_token',
  ];

  static function autenticar($data)
  {

    $sql = "SELECT
    user_id,
    firstname,
    lastname,
    username
    from user
    WHERE username LIKE '%{$data['username']}%'";

    return (array)DB::select($sql);

  }

  static function autenticarDev($data)
  {

    $sql = "SELECT
    u.user_id,
    u.firstname,
    u.lastname,
    u.username
    from user as u
    JOIN devs as d ON d.user_id = u.user_id
    WHERE u.username LIKE '%{$data['username']}%'";

    return (array)DB::select($sql);

  }


}
