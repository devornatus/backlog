<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Backlog;
use Session;


class BacklogController extends Controller
{
  function __construct()
  {
    if(session_id()=='')
    {
      session_start();
    }
  }

  function home(Request $request)
  {
    $data = array();

    $data['user_id'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id : 0;
    $data['filter_backlog'] = isset($_GET['filter_backlog']) ? $_GET['filter_backlog'] : '';
    $data['filter_system'] = isset($_GET['filter_system']) ? $_GET['filter_system'] : 0;

    $solicitacoes = Backlog::getBacklogsByUserId($data);

    $data['systems'] = Backlog::getSystems();
    $data['solicitacoes'] = $this->arrayPaginator($solicitacoes, $request);

    return view('home', $data);
  }

  function pageCreateSolicitation(Request $request)
  {
    $data = array();

    $data['sistemas'] = Backlog::getSystems();

    return view('solicitation-page', $data);
  }


  function createSolicitation(Request $request)
  {

    $data = array();
    $response = array();

    $response['response'] = false;
    $response['error_msg'] = "Não foi possível registrar a solicitação, tente novamente!";

    $data['images'] = isset($_POST['arquivos'])? $_POST['arquivos'] : '';
    $data['descricao'] = isset($_POST['descricao']) ? addslashes($_POST['descricao']) : '';
    $data['finalidade'] = isset($_POST['finalidade']) ? addslashes($_POST['finalidade']) : '';
    $data['sistema_id'] = isset($_POST['sistema_id']) ? $_POST['sistema_id'] : 0;
    $data['user_id'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id : 0;
    $data['status_id'] = 1;

    $data['solicitacao_id'] = Backlog::createSolicitation($data);

    if(count($data['solicitacao_id']) > 0)
    {
      Backlog::saveImagebySolicitationId($data);
      Backlog::logStatus($data);

      $response['response'] = true;
      $response['error_msg'] = "";
    }

    return Response::json($response);
  }


  public function arrayPaginator($array, $request)
  {

    $currentPage = LengthAwarePaginator::resolveCurrentPage();
    $page = $currentPage;
    $perPage = 10;
    $offset = ($page * $perPage) - $perPage;

    return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,['path' => $request->url(),'query' => $request->query()]);
  }

  function viewSolicitation(Request $request, $solicitacao_id)
  {
    $data = array();

    $data['solicitacao_id'] = isset($solicitacao_id) ? $solicitacao_id : 0 ;
    $data['user_id'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id : 0;

    $data['info_solicitacao'] = Backlog::getSolicitationById($data);
    $data['total_images'] = Backlog::getImagesBySolicitationIdAndUserId($data);
    $data['images'] = Backlog::getImagesBySolicitationId($data);
    $data['mensagens'] = Backlog::getMessagesSolicitationById($data);

    return view('view-solicitation', $data);
  }

  function homeDev(Request $request)
  {
    $data = array();

    $data['user_id'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id : 0;
    $data['filter_backlog'] = isset($_GET['filter_backlog']) ? $_GET['filter_backlog'] : '';
    $data['filter_system'] = isset($_GET['filter_system']) ? $_GET['filter_system'] : 0;
    $data['filter_status'] = isset($_GET['filter_status']) ? $_GET['filter_status'] : 99;
    $data['filter_dev'] = isset($_GET['filter_dev']) ? $_GET['filter_dev'] : 0;

    $solicitacoes = Backlog::getBacklogs($data);
    $data['total'] = count($solicitacoes);

    //gráfico
    $status_graphic = Backlog::getTotalBacklogs();

    foreach ($status_graphic as $status) {
      $status->color = trim('am4core.color("'.$status->color.'")');
    }

    $data['status_graphic'] =  json_encode($status_graphic);

    $data['systems'] = Backlog::getSystems();
    $data['status'] = Backlog::getStatus();
    $data['devs'] = Backlog::getDevs();

    $data['solicitacoes'] = $this->arrayPaginator($solicitacoes, $request);

    return view('homeDev', $data);
  }


  function viewSolicitationDev(Request $request, $solicitacao_id)
  {
    $data = array();

    $data['solicitacao_id'] = isset($solicitacao_id) ? $solicitacao_id : 0 ;

    $data['systems'] = Backlog::getSystems();
    $data['status'] = Backlog::getStatus();

    $data['info_solicitacao'] = Backlog::getSolicitationById($data);
    $data['mensagens'] = Backlog::getMessagesSolicitationById($data);
    $data['sqls'] = Backlog::getSqlSolicitationById($data);
    $data['devs'] = Backlog::getDevs();
    $data['images'] = Backlog::getImagesBySolicitationId($data);

    return view('view-solicitation-dev', $data);
  }

  function startSolicitation(Request $request)
  {

    $data = array();
    $response = array();

    $response['response'] = false;
    $response['error_msg'] = "Não foi possível iniciar a solicitação, tente novamente!";

    $data['descricao'] = isset($_POST['descricao']) ? addslashes($_POST['descricao']) : '';
    $data['data_entrega'] = isset($_POST['data_entrega']) ? $_POST['data_entrega'] : '';
    $data['status_id'] = isset($_POST['status_id']) ? $_POST['status_id'] : 0;
    $data['dev_id'] = isset($_POST['dev_id']) ? $_POST['dev_id'] : 0;
    $data['solicitacao_id'] = isset($_POST['solicitacao_id']) ? $_POST['solicitacao_id'] : 0;
    $data['user_id'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id : 0;

    $update = Backlog::startSolicitation($data);

    if(count($update) > 0)
    {
      Backlog::logStatus($data);
      Backlog::insertMessageBySolicitationId($data);

      $response['response'] = true;
      $response['error_msg'] = "";
    }

    return Response::json($response);
  }

  function insertMessageBySolicitationId(Request $request)
  {

    $data = array();
    $response = array();

    $response['response'] = false;
    $response['error_msg'] = "Não foi possível salvar esta mensagem, tente novamente!";

    $data['descricao'] = isset($_POST['descricao']) ? addslashes($_POST['descricao']) : '';
    $data['solicitacao_id'] = isset($_POST['solicitacao_id']) ? $_POST['solicitacao_id'] : 0;
    $data['user_id'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id : 0;

    $insert =   Backlog::insertMessageBySolicitationId($data);

    if($insert > 0)
    {
      $response['response'] = true;
      $response['error_msg'] = "";
    }

    return Response::json($response);
  }

  function insertSqlBySolicitationId(Request $request)
  {

    $data = array();
    $response = array();

    $response['response'] = false;
    $response['error_msg'] = "Não foi possível salvar este SQL, tente novamente!";

    $data['descricao_sql'] = isset($_POST['descricao_sql']) ? addslashes($_POST['descricao_sql']) : '';
    $data['solicitacao_id'] = isset($_POST['solicitacao_id']) ? $_POST['solicitacao_id'] : 0;
    $data['user_id'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id : 0;

    $insert =   Backlog::insertSqlBySolicitationId($data);

    if($insert > 0)
    {
      $response['response'] = true;
      $response['error_msg'] = "";
    }

    return Response::json($response);
  }

  function removeSqlBySolicitationId(Request $request)
  {

    $data = array();
    $response = array();

    $response['response'] = false;
    $response['error_msg'] = "Não foi possível remove este SQL, tente novamente!";

    $data['solicitacao_sql_id'] = isset($_POST['solicitacao_sql_id']) ? $_POST['solicitacao_sql_id'] : 0;
    $data['solicitacao_id'] = isset($_POST['solicitacao_id']) ? $_POST['solicitacao_id'] : 0;
    $data['user_id'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id : 0;

    $update =   Backlog::removeSqlBySolicitationId($data);

    if(count($update) > 0)
    {
      $response['response'] = true;
      $response['error_msg'] = "";
    }

    return Response::json($response);
  }


  function startDevelopBySolicitationId(Request $request)
  {

    $data = array();
    $response = array();

    $response['response'] = false;
    $response['error_msg'] = "Não foi possível iniciar o desenvolvimento desta solicitação, tente novamente!";

    $data['branch'] = isset($_POST['branch']) ? $_POST['branch'] : '';
    $data['dev_id'] = isset($_POST['dev_id']) ? $_POST['dev_id'] : 0;
    $data['solicitacao_id'] = isset($_POST['solicitacao_id']) ? $_POST['solicitacao_id'] : 0;
    $data['user_id'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id : 0;
    $data['status_id'] = 3;

    $update =   Backlog::startDevelopBySolicitationId($data);

    if(count($update) > 0)
    {
      Backlog::logStatus($data);
      $response['response'] = true;
      $response['error_msg'] = "";
    }

    return Response::json($response);
  }

  function homologationBySolicitationId(Request $request)
  {

    $data = array();
    $response = array();

    $response['response'] = false;
    $response['error_msg'] = "Não foi possível atualizar para homologação esta solicitação, tente novamente!";

    $data['solicitacao_id'] = isset($_POST['solicitacao_id']) ? $_POST['solicitacao_id'] : 0;
    $data['user_id'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id : 0;
    $data['status_id'] = 4;

    $update =   Backlog::homologationBySolicitationId($data);

    if(count($update) > 0)
    {
      Backlog::logStatus($data);
      $response['response'] = true;
      $response['error_msg'] = "";
    }

    return Response::json($response);
  }

  function reproveHomologationBySolicitationId(Request $request)
  {

    $data = array();
    $response = array();

    $response['response'] = false;
    $response['error_msg'] = "Não foi possível reprovar esta solicitação, tente novamente!";

    $data['solicitacao_id'] = isset($_POST['solicitacao_id']) ? $_POST['solicitacao_id'] : 0;
    $data['user_id'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id : 0;
    $data['status_id'] = 8;

    $update =   Backlog::reproveHomologationBySolicitationId($data);

    if(count($update) > 0)
    {
      Backlog::logStatus($data);
      $response['response'] = true;
      $response['error_msg'] = "";
    }

    return Response::json($response);
  }

  function aproveHomologationBySolicitationId(Request $request)
  {

    $data = array();
    $response = array();

    $response['response'] = false;
    $response['error_msg'] = "Não foi possível aprovar esta solicitação, tente novamente!";

    $data['solicitacao_id'] = isset($_POST['solicitacao_id']) ? $_POST['solicitacao_id'] : 0;
    $data['user_id'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id : 0;
    $data['status_id'] = 5;

    $update = Backlog::aproveHomologationBySolicitationId($data);

    if(count($update) > 0)
    {
      Backlog::logStatus($data);
      $response['response'] = true;
      $response['error_msg'] = "";
    }

    return Response::json($response);
  }

  function aproveBySolicitationId(Request $request)
  {

    $data = array();
    $response = array();

    $response['response'] = false;
    $response['error_msg'] = "Não foi possível alterar para produção esta solicitação, tente novamente!";

    $data['solicitacao_id'] = isset($_POST['solicitacao_id']) ? $_POST['solicitacao_id'] : 0;
    $data['user_id'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id : 0;
    $data['status_id'] = 6;

    $update = Backlog::aproveBySolicitationId($data);

    if(count($update) > 0)
    {
      Backlog::logStatus($data);
      $response['response'] = true;
      $response['error_msg'] = "";
    }

    return Response::json($response);
  }

  function finishBySolicitationId(Request $request)
  {

    $data = array();
    $response = array();

    $response['response'] = false;
    $response['error_msg'] = "Não foi possível finalizar sem desenvolver esta solicitação, tente novamente!";

    $data['solicitacao_id'] = isset($_POST['solicitacao_id']) ? $_POST['solicitacao_id'] : 0;
    $data['justificativa'] = isset($_POST['justificativa']) ? $_POST['justificativa'] : '';
    $data['user_id'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id : 0;
    $data['status_id'] = 7;

    $update = Backlog::finishBySolicitationId($data);

    if(count($update) > 0)
    {
      Backlog::logStatus($data);
      $response['response'] = true;
      $response['error_msg'] = "";
    }

    return Response::json($response);
  }

  function pageCreateSolicitationDev(Request $request)
  {
    $data = array();

    $data['sistemas'] = Backlog::getSystems();
    $data['users'] = Backlog::getUsers();

    return view('create-solicitation-dev', $data);
  }


  function createSolicitationDev(Request $request)
  {

    $data = array();
    $response = array();

    $response['response'] = false;
    $response['error_msg'] = "Não foi possível registrar a solicitação, tente novamente!";

    $data['images'] = isset($_POST['arquivos'])? $_POST['arquivos'] : '';
    $data['descricao'] = isset($_POST['descricao']) ? addslashes($_POST['descricao']) : '';
    $data['finalidade'] = isset($_POST['finalidade']) ? addslashes($_POST['finalidade']) : '';
    $data['sistema_id'] = isset($_POST['sistema_id']) ? $_POST['sistema_id'] : 0;
    $data['user_id'] = isset($_POST['user_id']) ? $_POST['user_id'] : 0;
    $data['status_id'] = 1;

    $response['solicitacao_id'] = Backlog::createSolicitationDev($data);

    $data['solicitacao_id'] = $response['solicitacao_id'];

    if(count($response['solicitacao_id']) > 0)
    {
      //altero para a sessão do dev, para poder inserir no log qual desenvolvedor criou o chamado
      $data['user_id'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id : 0;
      Backlog::logStatus($data);
      Backlog::saveImagebySolicitationId($data);

      $response['response'] = true;
      $response['error_msg'] = "";
    }

    return Response::json($response);
  }

  function searchDecription(Request $request)
  {
    $data = array();

    $data['filter_description'] = isset($_GET['filter_description']) ? $_GET['filter_description'] : '';
    $data['first_access'] = isset($_GET['first_access']) ? 1 : 0;

    if($data['first_access'] > 0)
    {
      $data['descriptions'] =  Backlog::searchDecription($data);
    }
    else
    {
      $data['descriptions'] =  array();
    }


    return view('searchDecription', $data);
  }

  function searchQuery(Request $request)
  {
    $data = array();

    $data['filter_query'] = isset($_GET['filter_query']) ? $_GET['filter_query'] : '';
    $data['first_access'] = isset($_GET['first_access']) ? 1 : 0;

    if($data['first_access'] > 0)
    {
      $data['descriptions'] =  Backlog::searchQuery($data);
    }
    else
    {
      $data['descriptions'] =  array();
    }


    return view('searchQuery', $data);
  }


  function uploadPhoto(Request $request)
  {

    $data = array();

    if (isset($_FILES["image"]))
    {
      $data['image'] = $_FILES['image'];

      //5mb em bytes
      $max_size =  '5145728';
      //1mb
      $min_size =  '1145728';

      if($_FILES['image']['size'] > $max_size)
      {
        $data['result'] = false;
        return Response::json($data);
      }



      $data['image']['extension'] = pathinfo($_FILES['image']['name'])['extension'] ;
      $data['image']['name'] = $_SESSION['usuario']->user_id.date('dmY_Hsi').".".$data['image']['extension'] ;
      $data['image']['tmp_name'] = $_FILES['image']['tmp_name'];
      $data['user_id'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id : 0 ;
      $data['image']['local_file'] = "./images/".date('Y-m')."/";

      if(!file_exists($data['image']['local_file'])){
        mkdir($data['image']['local_file']);
      }

      if( $data['image']['extension'] == 'jpeg' || $data['image']['extension'] == 'jpg' || $data['image']['extension'] == 'png' )
      {
        if($_FILES['image']['size'] > $min_size )
        {
          $file = $_FILES['image']['tmp_name'];

          list($width,$height) = getimagesize($file);

          $nwidth=800;
          $nheight=800;
          $newimage = imagecreatetruecolor($nwidth,$nheight);

          if($_FILES['image']['type']=='image/jpeg' || $_FILES['image']['type']=='image/jpg')
          {
            $source=imagecreatefromjpeg($file);
            imagecopyresized($newimage,$source,0,0,0,0,$nwidth,$nheight,$width,$height);
            $file_name = $data['image']['name'];

            imagejpeg($newimage,$data['image']['local_file'].$file_name);
          }
          elseif($_FILES['image']['type']=='image/png')
          {
            $source=imagecreatefrompng($file);
            imagecopyresized($newimage,$source,0,0,0,0,$nwidth,$nheight,$width,$height);
            $file_name = $data['image']['name'];

            imagepng($newimage,$data['image']['local_file'].$file_name);
          }

        }else{
          $result = move_uploaded_file($data['image']['tmp_name'], $data['image']['local_file'] . $data['image']['name']);

          if($result == 0 || $result == false)
          {
            return Response::json('false');
          }
        }



        $data['image_id'] = Backlog::savePhoto($data);
        $data['image'] = Backlog::getImageById($data['image_id']);

        $data['result'] = true;
        return Response::json($data);

      }else
      {

        $data['result'] = 'diff_file';
        return Response::json($data);

      }

    }else
    {
      $data['result'] = false;
      return Response::json($data);
    }

  }


  function uploadPhotoBySolicitationId(Request $request)
  {

    $data = array();

    if (isset($_FILES["image"]))
    {
      $data['image'] = $_FILES['image'];

      //5mb em bytes
      $max_size =  '5145728';
      //1mb
      $min_size =  '1145728';

      if($_FILES['image']['size'] > $max_size)
      {
        $data['result'] = false;
        return Response::json($data);
      }

      $data['solicitacao_id'] = isset($_POST['solicitacao_id']) ? $_POST['solicitacao_id'] : 0 ;
      $data['image']['extension'] = pathinfo($_FILES['image']['name'])['extension'] ;
      $data['image']['name'] = $_SESSION['usuario']->user_id.date('dmY_Hsi').".".$data['image']['extension'] ;
      $data['image']['tmp_name'] = $_FILES['image']['tmp_name'];
      $data['user_id'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id : 0 ;
      $data['image']['local_file'] = "./images/".date('Y-m')."/";

      if(!file_exists($data['image']['local_file'])){

        mkdir($data['image']['local_file']);
      }

      if( $data['image']['extension'] == 'jpeg' || $data['image']['extension'] == 'jpg' || $data['image']['extension'] == 'png' )
      {
        if($_FILES['image']['size'] > $min_size )
        {
          $file = $_FILES['image']['tmp_name'];

          list($width,$height) = getimagesize($file);

          $nwidth=800;
          $nheight=800;
          $newimage = imagecreatetruecolor($nwidth,$nheight);

          if($_FILES['image']['type']=='image/jpeg' || $_FILES['image']['type']=='image/jpg')
          {
            $source=imagecreatefromjpeg($file);
            imagecopyresized($newimage,$source,0,0,0,0,$nwidth,$nheight,$width,$height);
            $file_name = $data['image']['name'];

            imagejpeg($newimage,$data['image']['local_file'].$file_name);
          }
          elseif($_FILES['image']['type']=='image/png')
          {
            $source=imagecreatefrompng($file);
            imagecopyresized($newimage,$source,0,0,0,0,$nwidth,$nheight,$width,$height);
            $file_name = $data['image']['name'];
        
            imagepng($newimage,$data['image']['local_file'].$file_name);
          }

        }else{
          $result = move_uploaded_file($data['image']['tmp_name'], $data['image']['local_file'] . $data['image']['name']);

          if($result == 0 || $result == false)
          {
            return Response::json('false');
          }
        }



        $data['image_id'] = Backlog::savePhotoWithSolicitationId($data);
        $data['image'] = Backlog::getImageById($data['image_id']);

        $data['result'] = true;
        return Response::json($data);

      }else
      {

        $data['result'] = 'diff_file';
        return Response::json($data);

      }

    }else
    {
      $data['result'] = false;
      return Response::json($data);
    }

  }


  function updateSolicitation(Request $request)
  {

    $data = array();
    $response = array();

    $response['response'] = false;
    $response['error_msg'] = "Não foi possível atualizar a solicitação, tente novamente!";

    $data['images'] = isset($_POST['arquivos']) ? $_POST['arquivos'] : '';
    $data['solicitacao_id'] = isset($_POST['solicitacao_id']) ? $_POST['solicitacao_id'] : 0;
    $data['descricao'] = isset($_POST['descricao']) ? addslashes($_POST['descricao']) : '';
    $data['user_id'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id : 0;

    if(strlen($data['descricao']) > 5)
    {
      Backlog::insertMessageBySolicitationId($data);
    }


    if(count($data['solicitacao_id']) > 0)
    {
      if(isset($data['images'][0]) && count($data['images']) > 0 &&  $data['images'][0] > 0)
      {
        Backlog::saveImagebySolicitationId($data);
      }
    }


    $response['response'] = true;
    $response['error_msg'] = "";

    return Response::json($response);
  }

  function linkImagebySolicitationId(Request $request)
  {

    $data = array();
    $response = array();

    $response['response'] = false;
    $response['error_msg'] = "Não foi possível vincular imagem a solicitação, tente novamente!";

    $data['image_id'] = isset($_POST['image_id']) ? $_POST['image_id'] : 0;
    $data['solicitacao_id'] = isset($_POST['solicitacao_id']) ? $_POST['solicitacao_id'] : 0;
    $data['user_id'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id : 0;

    if(count($data['image_id']) > 0)
    {
      $update =  Backlog::linkImagebySolicitationId($data);

      if(count($update) == 0)
      {
        return Response::json($response);
      }

    }
    else
    {
      return Response::json($response);
    }


    $response['response'] = true;
    $response['error_msg'] = "";

    return Response::json($response);
  }

  function removeImage(Request $request)
  {

    $data = array();
    $response = array();

    $response['response'] = false;
    $response['error_msg'] = "Não foi possível remover imagem, tente novamente!";

    $data['image_id'] = isset($_POST['image_id']) ? $_POST['image_id'] : 0;
    $data['solicitacao_id'] = isset($_POST['solicitacao_id']) ? $_POST['solicitacao_id'] : 0;
    $data['user_id'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id : 0;

    if(count($data['image_id']) > 0)
    {
      $update =  Backlog::removeImage($data);

      if(count($update) == 0)
      {
        return Response::json($response);
      }

    }
    else
    {
      return Response::json($response);
    }


    $response['response'] = true;
    $response['error_msg'] = "";

    return Response::json($response);
  }


}
