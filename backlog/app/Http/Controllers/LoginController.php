<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use App\User;
use Session;


class LoginController extends Controller
{
  function __construct()
  {
    if(session_id()=='')
    {
      session_start();
    }
  }

  function login(Request $request)
  {
    return view('login');
  }


  function autenticar(Request $request)
  {

    $data = array();
    $response = array();

    $response['response'] = false;
    $response['error_msg'] = "Não foi possível realizar o login, tente novamente!";

    $data['username'] = isset($_POST['username']) ? $_POST['username'] : '';

    $autenticar = User::autenticar($data);

    if(count($autenticar) > 0)
    {
      $_SESSION['usuario'] = $autenticar[0];

      $response['response'] = true;
      $response['error_msg'] = "";
    }

    return Response::json($response);
  }

  function logout(Request $request)
  {

    unset($_SESSION['usuario']);
    session_destroy();
    return view('login');

  }

  function sessionExpired()
  {
    session_destroy();

    return view('login');

  }

  function loginDev(Request $request)
  {
    return view('loginDev');
  }

  function autenticarDev(Request $request)
  {

    $data = array();
    $response = array();

    $response['response'] = false;
    $response['error_msg'] = "Não foi possível realizar o login, tente novamente!";

    $data['username'] = isset($_POST['username']) ? $_POST['username'] : '';

    $autenticar = User::autenticarDev($data);

    if(count($autenticar) > 0)
    {
      $_SESSION['usuario'] = $autenticar[0];

      $response['response'] = true;
      $response['error_msg'] = "";
    }

    return Response::json($response);
  }

  function logoutDev(Request $request)
  {

    unset($_SESSION['usuario']);
    session_destroy();
    return view('loginDev');

  }

}
