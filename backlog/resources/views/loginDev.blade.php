<!DOCTYPE html>
<html lang="en">
<head>
	<title>Grupo Ornatus - TI Tasks</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="{{ asset('assets/images/icons/favicon.ico') }}"/>
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/fonts/iconic/css/material-design-iconic-font.min.css') }}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/animate/animate.css') }}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/css-hamburgers/hamburgers.min.css') }}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/animsition/css/animsition.min.css') }}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/select2/select2.min.css') }}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/daterangepicker/daterangepicker.css') }}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/util.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/main.css') }}">
	<!--===============================================================================================-->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>


	<link href="https://fonts.googleapis.com/css2?family=KoHo&display=swap" rel="stylesheet">

</head>
<body>

	<style>
	@media only screen and (max-width: 600px) {
		#logo {
			width:90%;
			height:90%;
			margin:15px;
		}
	}

	@media only screen and (min-width: 601px) {
		#logo {
			width:50%;
			height:50%;
			margin-left:95px;
		}
	}

	.fundo {
 background-image: url("{{ asset('assets/images/background.jpg') }}") !important;
}

	</style>


	<div class="limiter fundo">
		<div class="container-login100">
			<div class="wrap-login100 p-t-85 p-b-20">


				<img id="logo" src="{{ asset('assets/images/grupoornatus.png') }}" alt="morana">

				<div style="margin-top:30px; margin-bottom:-30px;" align="center">
					<span style="font-family: 'KoHo', sans-serif; font-size:40pt;">TI-TASKS <b style="color:#0a9120;">DEV</b></span>
				</div>

								<div style=" border: 1px solid #cfcfcf; padding: 15px; box-shadow: 5px 5px #888888; margin-top:30px; border-radius:10px;">
				<div  class="wrap-input100 validate-input m-t-85 m-b-35" >
					<input class="" type="text" id="username" placeholder="Usuário" style="width:100%;" />
				</div>

				<div class="container-login100-form-btn">
					<button type="button" id="btn-login" class="btn" style="background-color:grey; color:white; border-radius:20px; width:70%; height:50px; font-size:15pt;">
						Login
					</button>
				</div>
			</div>

			</div>
		</div>
	</div>

	<script>
	jQuery(document).ready(function($) {

		$('#btn-login').click(function (event) {

			$('#btn-login').attr('disabled',true);

			username = $('#username').val();

			if(username.length <= 5){
				swal("Atenção", "Usuário Inválido", "");
				$('#btn-login').attr('disabled',false);
				return false;
			}

			var url = "{{ URL::to('/autenticar_dev') }}";

			$.ajax({
				url: url,
				method:'POST',
				data: {
					username:username,
					"_token":"{{ csrf_token() }}"
				},
				success: function(data, textStatus, xhr)
				{

					if(data.response == 'true' || data.response == true)
					{
						window.location.href = "{{ URL::to('/home_dev') }}";
					}
					else
					{
						swal("Erro!", data.error_msg, "error");
						$('#btn-login').attr('disabled',false);
						return false;
					}

				},
				error: function(xhr, status, error)
				{
					swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
					$('#btn-login').attr('disabled',false);
				}

			})
			.done(function( data )
			{

			})
			.fail(function( jqXHR ) {
				swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
				$('#btn-login').attr('disabled',false);
			});



		});

	})


</script>

<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
<script src="{{ asset('assets/vendor/jquery/jquery-3.2.1.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('assets/vendor/animsition/js/animsition.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('assets/vendor/bootstrap/js/popper.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('assets/vendor/select2/select2.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('assets/vendor/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('assets/vendor/daterangepicker/daterangepicker.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('assets/vendor/countdowntime/countdowntime.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('assets/js/main.js') }}"></script>

</body>
</html>
