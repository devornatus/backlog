@include('header')

<style>
@media only screen and (max-width: 500px) {

}


#imagem {
  width: 100px;
  height 200px;
}

#texto {
  position: absolute;
  margin-top: -400px;
}

#texto2 {
  position: absolute;
  margin-top: -30px;

}
</style>

<div class="portfolio-modal modal fade" tabindex="-1" id="portfolioModal1" role="dialog" aria-labelledby="portfolioModal1Label" aria-hidden="true">

  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true"><i class="fas fa-times"></i></span>
      </button>
      <div class="modal-body text-center">
        <div class="row justify-content-center" id="portfolioModal">

        </div>
      </div>
    </div>
  </div>
</div>

<section class="page-section portfolio" id="portfolio" style="margin-top:10px;">
  <div class="container">
    <div class="row" style="margin-top:20px; margin-bottom:20px;">
      <div class="col-lg-12" align="center">
        <br>
        <h2>Minhas Solicitações</h2>
      </div>
    </div>
    <div class="row" id="div_posts" style="margin-top:30px;">
      <div class="col-lg-12" style="margin-bottom:10px;">
        <form name="form_filtro"  method="GET">
          <table  style="width:100%;">
            <tr>
              <td style="width:80%; padding-right:10px;">
                <input id="filter_backlog" name="filter_backlog" type="text" class="form-control" value="<?php echo isset($filter_backlog) ? $filter_backlog : '' ; ?>" />
              </td>
              <td style="width:80%; padding-right:10px;">
                <select class="form-control" id="filter_system" name="filter_system">
                  <option value="0">Selecionar</option>
                  <?php foreach ($systems as $sistem) { ?>
                    <option <?php echo ($filter_system == $sistem->sistema_id) ? 'selected' : '';?> value="<?php echo $sistem->sistema_id;?>"><?php echo $sistem->descricao;?></option>
                  <?php } ?>
                </select>
              </td>
              <td>
                <button class="btn btn-primary" type="submit" id="filtrar">Filtrar</button>
              </td>
            </tr>
          </table>
        </form>
      </div>
      <div align="center">
        {{ $solicitacoes->render()}}
      </div>
      <table class="table">
        <thead>
          <th>Nº Solicitação</th>
          <th>Plataforma</th>
          <th>Descrição</th>
          <th>Data Criação</th>
          <th>Data Entrega</th>
          <th>Data Produção</th>
          <th>Status</th>
          <th>Ações</th>
        </thead>
        <?php foreach ($solicitacoes as $solicitacao) { ?>
          <tr>
            <td><?php echo $solicitacao->solicitacao_id;?></td>
            <td><?php echo $solicitacao->nome_plataform;?></td>
            <td><?php echo substr($solicitacao->descricao, 0, 50);?>...</td>
            <td><?php echo date("d/m/Y", strtotime($solicitacao->date_added)); ?></td>
            <td><?php echo ($solicitacao->data_entrega) ? date("d/m/Y", strtotime($solicitacao->data_entrega)) : "" ; ?></td>
            <td><?php echo ($solicitacao->data_producao) ? date("d/m/Y", strtotime($solicitacao->data_producao)) : "" ; ?></td>
            <td><?php echo $solicitacao->descricao_status;?></td>
            <td>
              <a href="{{ URL::to('/visualizar/'.$solicitacao->solicitacao_id) }}" class="btn btn-warning" style="color:white;">
                <i class="fa fa-eye" aria-hidden="true"></i>
              </a>
            </td>
          </tr>
        <?php } ?>
      </table>
      <div align="center">
        {{ $solicitacoes->render()}}
      </div>
    </div>

  </div>


</section>



@include('footer')

<script>

$(document).ready(function () {

  $('#form_filtro').click(function(){
    $('#form_filtro').attr("disabled", true);
    $('#form_filtro').submit();

  });


})

</script>
