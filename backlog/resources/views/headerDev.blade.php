<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <title>Grupo Ornatus - TI Tasks</title>

  <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />

  <script src="{{ asset('js/sweetalert/distr/sweetalert.min.js') }}"></script>
  <link href="{{ asset('js/sweetalert/distr/sweetalert.css') }}" rel="stylesheet">

  <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>

  <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
  <script src="https://cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>
  <script src="https://code.jquery.com/jquery-2.2.4.js"></script>

  <script src="https://cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>
  <script src="" ></script>
  <link href="{{ asset('assets/css/styles.css') }}" rel="stylesheet" />

  <script src="{{ asset('assets/admcharts/core.js') }}"></script>
  <script src="{{ asset('assets/admcharts/charts.js') }}"></script>
  <script src="{{ asset('assets/admcharts/themes/dataviz.js') }}"></script>
  <script src="{{ asset('assets/admcharts/themes/animated.js') }}"></script>
  <script src="{{ asset('assets/admcharts/themes/material.js') }}"></script>

</head>
<body id="page-top">

  <div class="portfolio-modal modal fade" tabindex="-1" id="portfolioModal2" role="dialog" aria-labelledby="portfolioModal2Label" aria-hidden="true">

    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-times"></i></span>
        </button>
        <div class="modal-body text-center">
          <div class="row justify-content-center" id="portfolioModal_teste">

          </div>
        </div>
      </div>
    </div>
  </div>


  <style>
  @media only screen and (max-width: 500px) {
    #btn_bell {
      margin-left:-17px;
    }
  }

  </style>

  <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="{{ URL::to('/home_dev') }}">
        <img src="{{ asset('assets/images/logobranco.png') }}" height="70px">
      </a>
      <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <i  style="font-size:11pt;" class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <?php if(isset($_SESSION['usuario']->user_id)){?>
          <ul class="navbar-nav ml-auto">

            <li class="nav-item mx-0 mx-lg-1" style="text-align:center">
              <p style="font-size:15pt;color:white; margin-top:25px;">Olá, <?php echo isset($_SESSION['usuario']->firstname) ? $_SESSION['usuario']->firstname : ''; ?></p>
            </li>

            <li class="nav-item mx-0 mx-lg-1" style="text-align:center">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="{{ URL::to('/search-descripition') }}"><i style="font-size:20pt; color:white;" class="grow fa fa-search" aria-hidden="true" title="Post para sugestão de preço e código brasil">
              </i><p style="font-size:8pt;color:white;">Procurar Descrição</p>
            </a>
            </li>
            <li class="nav-item mx-0 mx-lg-1" style="text-align:center">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="{{ URL::to('/search-query') }}"><i style="font-size:20pt; color:white;" class="grow fa fa-database" aria-hidden="true" title="Post para sugestão de preço e código brasil">
              </i><p style="font-size:8pt;color:white;">Procurar Query</p>
            </a>
          </li>
            <li class="nav-item mx-0 mx-lg-1" style="text-align:center">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="{{ URL::to('/criar-solicitacao-dev') }}"><i style="font-size:20pt; color:white;" class="grow fa fa-exclamation-triangle" aria-hidden="true" title="Post para sugestão de preço e código brasil">
              </i><p style="font-size:8pt;color:white;">Criar Solicitação</p>
            </a>
          </li>

          <li class="nav-item mx-0 mx-lg-1" style="text-align:center">
            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="{{ URL::to('/logout_dev') }}"><i style="font-size:20pt; color:#0ac48d;" class="grow fas fa-sign-out-alt" aria-hidden="true" title="Sair"></i>
              <p style="font-size:8pt;color:#0ac48d;">Sair</p>
            </a>

          </li>
        </ul>

      <?php } ?>

    </div>
  </div>
</nav>



<style>

.grow:hover
{
  -webkit-transform: scale(1.3);
  -ms-transform: scale(1.3);
  transform: scale(1.3);
}


</style>


<script>
jQuery(document).ready(function($) {



})
</script>
