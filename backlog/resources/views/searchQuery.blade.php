@include('headerDev')

<style>
@media only screen and (max-width: 500px) {

}


#imagem {
  width: 100px;
  height 200px;
}

#texto {
  position: absolute;
  margin-top: -400px;
}

#texto2 {
  position: absolute;
  margin-top: -30px;

}
</style>

<section class="page-section portfolio" id="portfolio" style="margin-top:10px;">
  <div class="container">
    <div class="row" style="margin-top:20px; margin-bottom:20px;">
      <div class="col-lg-12" align="center">
        <br>
        <h2>Procurar por Query</h2>
      </div>
    </div>
    <div class="row" id="div_posts" style="margin-top:30px;">
      <div class="col-lg-12" style="margin-bottom:10px;">
        <form name="form_filtro"  method="GET">
          <table  style="width:100%;">
            <tr>
              <td style="width:90%; padding-right:10px;">
                <input id="filter_query" name="filter_query" type="text" class="form-control" value="<?php echo isset($filter_query) ? $filter_query : '' ; ?>" />
              </td>
              <input name="first_access" id="first_access" value="1" class="d-none" />
              <td>
                <button class="btn btn-primary"  id="filtrar">Filtrar</button>
              </td>
            </tr>
          </table>
        </form>
      </div>

      <table class="table">
        <thead>
          <th>Nº Solicitação</th>
          <th>Descrição</th>
          <th>Ações</th>
        </thead>
        <tbody>
          <?php foreach($descriptions as $description){ ?>
            <tr>
              <td>
                <?php echo $description->solicitacao_id;?>
              </td>
              <td>
                <?php echo $description->texto;?>
              </td>
              <td>
                <a href="{{ URL::to('/view_solicitation/'.$description->solicitacao_id) }}" class="btn btn-warning" style="color:white;">
                  <i class="fa fa-eye" aria-hidden="true"></i>
                </a>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>

    </div>

  </div>


</section>



@include('footerDev')

<script>

$(document).ready(function () {

  $('#filtrar').click(function(){

    filter_query = $('#filter_query').val();

    if(filter_query.length < 4)
    {
      swal("Atenção", "Filtro deve conter +4 caracteres!", "info");
      return false;
    }

    $('#form_filtro').submit();

  });


})

</script>
