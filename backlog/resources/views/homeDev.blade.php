@include('headerDev')

<style>
@media only screen and (max-width: 500px) {

}

#chartdiv {
  width: 100%;
  height: 200px;
  padding-top:20px;
}

</style>


<section class="page-section portfolio" id="portfolio" style="margin-top:10px;">
  <div class="container">
    <div class="row" style="margin-top:20px; margin-bottom:20px;">
      <div class="col-lg-12" align="left">
        <br>
        <div id="chartdiv"></div>
        <br>
        <h2>Lista de Solicitações</h2>
      </div>
    </div>
    <div class="row" id="div_posts" style="margin-top:30px;">
      <div class="col-lg-12" style="margin-bottom:10px;">
        <form name="form_filtro"  method="GET">
          <table  style="width:100%;">
            <tr>
              <td style="width:30%; padding-right:10px; padding-top:25px;">
                <input id="filter_backlog" placeholder="Nome, Email ou Descrição" name="filter_backlog" type="text" class="form-control" value="<?php echo isset($filter_backlog) ? $filter_backlog : '' ; ?>" />
              </td>
              <td style="width:20%; padding-right:10px;">
                <span>Desenvolvedor:</span>
                <select class="form-control" id="filter_dev" name="filter_dev">
                  <option value="0">Todos</option>
                  <?php foreach ($devs as $dev) { ?>
                    <option <?php echo ($filter_dev == $dev->user_id) ? 'selected' : '';?> value="<?php echo $dev->user_id;?>"><?php echo $dev->username;?></option>
                  <?php } ?>
                </select>
              </td>
              <td style="width:20%; padding-right:10px;">
                <span>Sistema:</span>
                <select class="form-control" id="filter_system" name="filter_system">
                  <option value="0">Todos</option>
                  <?php foreach ($systems as $sistem) { ?>
                    <option <?php echo ($filter_system == $sistem->sistema_id) ? 'selected' : '';?> value="<?php echo $sistem->sistema_id;?>"><?php echo $sistem->descricao;?></option>
                  <?php } ?>
                </select>
              </td>
              <td style="width:20%; padding-right:10px;">
                <span>Status:</span>
                <select class="form-control" id="filter_status" name="filter_status">
                  <option value="0">Em Analise, Ag. Desenvolvimento, Em Desenvolvimento</option>
                  <?php foreach ($status as $stat) { ?>
                    <option <?php echo ($filter_status == $stat->status_id) ? 'selected' : '';?> value="<?php echo $stat->status_id;?>"><?php echo $stat->descricao;?></option>
                  <?php } ?>
                </select>
              </td>
              <td>
                <button class="btn btn-primary" type="submit" id="filtrar" style="margin-top:25px;">Filtrar</button>
              </td>
            </tr>
          </table>
        </form>
      </div>
      <div align="center" style="margin:30px;">
        <b>Resultados:</b>(<?php echo $total;?>)
        <br>
        <br>
        {{ $solicitacoes->render()}}
      </div>
      <table class="table">
        <thead>
          <th>Nº</th>
          <th>Usuário</th>
          <th>Plataforma</th>
          <th>Descrição</th>
          <th>Data Criação</th>
          <th>Data Entrega</th>
          <th>Data Produção</th>
          <th>Status</th>
          <th>Responsável</th>
          <th>Ações</th>
        </thead>
        <?php foreach ($solicitacoes as $solicitacao) { ?>
          <tr>
            <td><?php echo $solicitacao->solicitacao_id;?></td>
            <td><?php echo $solicitacao->username;?></td>
            <td><?php echo $solicitacao->nome_plataform;?></td>
            <td><?php echo substr(htmlspecialchars_decode($solicitacao->descricao), 0, 60);?>...</td>
            <td><?php echo date("d/m/Y", strtotime($solicitacao->date_added)); ?></td>
            <td><?php echo ($solicitacao->data_entrega) ? date("d/m/Y", strtotime($solicitacao->data_entrega)) : "" ; ?></td>
            <td><?php echo ($solicitacao->data_producao) ? date("d/m/Y", strtotime($solicitacao->data_producao)) : "" ; ?></td>
            <td style="color:<?php echo $solicitacao->color; ?>; "><b><?php echo $solicitacao->descricao_status;?></b></td>
            <td><?php echo $solicitacao->dev_name;?></td>
            <td>
              <a href="{{ URL::to('/view_solicitation/'.$solicitacao->solicitacao_id) }}" class="btn btn-warning" style="color:white;">
                <i class="fa fa-eye" aria-hidden="true"></i>
              </a>
            </td>
          </tr>
        <?php } ?>
      </table>
      <div align="center" style="margin:30px;">
        {{ $solicitacoes->render()}}
      </div>
    </div>

  </div>


</section>



@include('footerDev')

<script>

$(document).ready(function () {

  $('#form_filtro').click(function(){
    $('#form_filtro').attr("disabled", true);
    $('#form_filtro').submit();

  });


})

</script>

<script>
am4core.ready(function() {

// Themes begin
am4core.useTheme(am4themes_material);
am4core.useTheme(am4themes_animated);
// Themes end

var chart = am4core.create("chartdiv", am4charts.PieChart);
chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

chart.data = <?php echo $status_graphic; ?>;
chart.radius = am4core.percent(70);
chart.innerRadius = am4core.percent(40);
chart.startAngle = 180;
chart.endAngle = 360;

var series = chart.series.push(new am4charts.PieSeries());
series.dataFields.value = "value";
series.dataFields.category = "status_name";

//series.slices.template.propertyFields.fill = "color";
series.slices.template.cornerRadius = 10;
series.slices.template.innerCornerRadius = 7;
series.slices.template.draggable = true;
series.slices.template.inert = true;
series.alignLabels = false;

series.hiddenState.properties.startAngle = 90;
series.hiddenState.properties.endAngle = 90;

chart.legend = new am4charts.Legend();

});
</script>
