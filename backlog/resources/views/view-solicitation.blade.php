@include('header')

<style>
@media only screen and (max-width: 500px) {

}

</style>



<section class="page-section portfolio" id="portfolio" style="margin-top:10px;">
  <div class="container">
    <div class="row" style="margin-top:20px; margin-bottom:20px;">
      <div class="col-lg-12" align="">
        <br>
        <h2>Solicitação: <?php echo $info_solicitacao[0]->solicitacao_id; ?></h2>
        <a href="{{ URL::to('/home') }}"  class="btn btn-primary" type="button" style="float:right;">
          Voltar
        </a>
      </div>
    </div>
    <div class="row" id="div_posts" style="margin-top:30px;">

      <h4>Olá, <?php echo isset($_SESSION['usuario']->firstname) ? $_SESSION['usuario']->firstname : '';?></h4>

      <div class="col-lg-12" style="margin-top:20px;">
        <span><b>Sistema:</b></span>
        <?php echo $info_solicitacao[0]->descricao_sistema;?>
      </div>
      <div class="col-lg-4" style="margin-top:20px;">
        <span><b>Data Criação:</b></span>
        <br>
        <?php echo date("d/m/Y", strtotime($info_solicitacao[0]->date_added));?>
      </div>
      <div class="col-lg-4" style="margin-top:20px;">
        <span><b>Data Entrega:</b></span>
        <br>
        <?php echo ($info_solicitacao[0]->data_entrega) ? date("d/m/Y", strtotime($info_solicitacao[0]->data_entrega)) : '';?>
      </div>
      <div class="col-lg-4" style="margin-top:20px;">
        <span><b>Data Produção:</b></span>
        <br>
        <?php echo ($info_solicitacao[0]->data_producao) ? date("d/m/Y", strtotime($info_solicitacao[0]->data_producao)) : '';?>
      </div>
      <div class="col-lg-12" style="margin-top:20px;">
        <span><b>Qual a solicitação ?</b></span>
        <br>
        <?php echo $info_solicitacao[0]->descricao;?>
      </div>

      <div class="col-lg-12" style="margin-top:20px;">
        <span><b>Para qual finalidade ?</b></span>
        <br>
        <?php echo $info_solicitacao[0]->finalidade;?>
      </div>

      <?php if($info_solicitacao[0]->status_id == 7){ ?>
        <div class="col-lg-12" style="margin-top:20px;">
          <span><b>Justificativa:</b></span>
          <br>
          <?php echo $info_solicitacao[0]->justificativa;?>
        </div>
      <?php } ?>

      <div class="col-lg-12"  style="margin-top:20px;">
        <span><b>Adicionar Mensagem:</b></span>
        <textarea id="descricao" name="" rows="4" cols="50" class="form-control"></textarea>
      </div>


      <?php if(count($total_images) < 3 ){

        $faltam = 3 - count($total_images);

        for($i = 1; $i <= $faltam; $i++){ ?>
          <div class="col-lg-12" align="center" style="margin-top:30px; border:3px solid #ced4da; border-radius:5px; padding:20px;">
            <div id="img<?php echo $i; ?>" class="box" >
            </div>
            <label class="fileContainer addFile<?php echo $i; ?>" style="margin:-10px; border-radius: 5px; text-align-last: center;  width:200px; height:200px;"  >
              <i style="font-size:30pt; margin-top:35%" class="fa fa-camera" aria-hidden="true"></i>
              <input name="image" class="image inputFile<?php echo $i; ?>" type="file" accept="image/*" data-id="<?php echo $i; ?>">
              <br>
              <b>Inserir Imagem</b>
            </label>
            <label class="fileContainer deleteFile<?php echo $i; ?> d-none" style=" font-size:10pt; height: 37px !important; margin-top:3px; background-color:red !important; width:130px; text-align:center !important; border-radius: 20px;" data-id="<?php echo $i; ?>" onclick="deleteFile(<?php echo $i; ?>);">Deletar
            </label>
            <input class="textimg<?php echo $i; ?> d-none" name="image[]" data-id="<?php echo $i; ?>" type="text" value="" />
          </div>

        <?php } ?>
      <?php } ?>

      <?php if(count($images) > 0 ){ ?>
        <div class="col-lg-12" style="margin-top:20px;">
          <span><b>Imagens:</b></span>
          <table class="table">

            <tbody>
              <?php foreach ($images as $image) { ?>
                <tr>

                  <td align="center">
                    <a href="<?php echo "../".$image->local_file.$image->name?>" target="_blank">
                      <img class="img-fluid img-thumbnail" style=" max-height: 200px;  max-width: 200px; border-radius:10px;"  src="<?php echo "../".$image->local_file.$image->name?>" alt="" />
                    </a>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>

        </div>
      <?php } ?>

      <div class="col-lg-12" style="margin-top:40px; ">
        <b><span style="font-size:20pt;">Mensagens:</span></b>

        <div style="width: 100%; height: 300px; overflow-y: scroll;" >
          <table class="table">
            <?php foreach ($mensagens as $mensagem) { ?>
              <tr>
                <td>
                  <b><?php echo $mensagem->username." - ".date("d/m/Y", strtotime($mensagem->date_added)); ?>: </b>
                </td>
                <td>
                  <?php echo $mensagem->texto;?>
                </td>
              </tr>
            <?php } ?>
          </table>
        </div>
      </div>

      <div class="col-lg-12" style="margin-top:20px;">
        <button type="button" class="btn btn-success btn-lg btn-block" id="update">Atualizar</button>
      </div>

      <div class="col-lg-12" style="margin-top:20px;">
        <a href="{{ URL::to('/home') }}"  class="btn btn-primary btn-lg btn-block" type="button" >
          Voltar
        </a>
      </div>

    </div>

  </div>





</section>



@include('footer')

<script>

function deleteFile(id){

  $('.textimg'+id).val('');
  $('.addFile'+id).removeClass('d-none');
  $('.addFile'+id).addClass('show');
  $('#img'+id).html('');
  $('.deleteFile'+id).addClass('d-none');
  $('.deleteFile'+id).removeClass('show');
  $('.inputFile'+id).val('');

}


$(document).ready(function () {

  $('#update').click(function (event) {

    $('#update').attr('disabled',true);

    swal({
      title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
      text: 'Aguarde ...',
      html: true,
      showCancelButton: false,
      showConfirmButton: false,
      closeOnConfirm: false,
      closeOnCancel: false
    });

    descricao = $('#descricao').val();

    var solicitacao_id = <?php echo $info_solicitacao[0]->solicitacao_id; ?>;

    var arquivos = [];
    var lista_arquivos = document.getElementsByName("image[]");

    for(var i =0; i< lista_arquivos.length;i++){
      arquivos.push(lista_arquivos[i].value);
    }

    if(descricao.length > 200){
      swal("Atenção!", "Campo de descrição deve conter no máximo 200 caracteres!", "");
      $('#update').attr('disabled',false);
      return false;
    }

    var url = "{{ URL::to('/updateSolicitation') }}";

    $.ajax({
      url: url,
      method:'POST',
      data: {
        descricao:descricao,
        arquivos:arquivos,
        solicitacao_id:solicitacao_id,
        "_token":"{{ csrf_token() }}"
      },
      success: function(data, textStatus, xhr)
      {

        if(data.response == 'true' || data.response == true)
        {
          swal({
            title: "Sucesso !",
            text: "Solicitação atualizada com sucesso!",
            type: "success",
            showCancelButton: false,
            confirmButtonText: "Sim",
            cancelButtonText: "Não",
            allowOutsideClick: false,
            closeOnConfirm: false,

          },
          function() {
            location.reload();
          });
        }
        else
        {
          swal("Erro!", data.error_msg, "error");
          $('#update').attr('disabled',false);
          return false;
        }

      },
      error: function(xhr, status, error)
      {
        swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
        $('#update').attr('disabled',false);
      }

    })
    .done(function( data )
    {

    })
    .fail(function( jqXHR ) {
      swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
      $('#update').attr('disabled',false);
    });



  });

  $('.image').change(function (event) {

    swal({
      title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
      text: 'Aguarde ...',
      html: true,
      showCancelButton: false,
      showConfirmButton: false,
      closeOnConfirm: false,
      closeOnCancel: false
    });

    var btn = $(this);
    var id_button_file = $(this).data('id');

    $("#base_image").val('');
    form = new FormData();

    form.append('image', event.target.files[0]);
    form.append('_token',"{{ csrf_token() }}");


    var url = "{{ URL::to('/uploadPhoto/') }}";

    $.ajax({
      url: url,
      cache:false,
      contentType:false,
      processData: false,
      data: form,
      type: 'POST',
      success: function (data) {

        if(data.result == 'false' || data.result == false){
          swal("Atenção", "Enviar arquivos somente de até 3Mbs.", "info");
          return false;
        }

        if(data.result == 'diff_file' ){
          swal("Atenção", "Enviar arquivos devem conter os formatos: PNG, JPG, JPEG e GIF.", "info");
          return false;
        }

        var obj = data;

        $('#img'+ id_button_file).html(' <img style=" max-height: 200px;  max-width: 200px;" class="img_new img-fluid"  src=../'+ data.image[0].local_file + '' + data.image[0].name + ' ></img>' );

        $('.textimg'+id_button_file).val(data.image[0].image_id);
        $('.addFile'+id_button_file).addClass('d-none');
        $('.addFile'+id_button_file).removeClass('show');
        $('.deleteFile'+id_button_file).removeClass('d-none');
        $('.deleteFile'+id_button_file).addClass('show');

        swal.close();
      }
    });

  });

})


</script>

<style>

.navbar2 {
  overflow: hidden;
  background-color: #333;
  position: fixed;
  bottom: 0;
  padding:10px;
  width: 100%;
}

.navbar2 a:hover {
  background: #ddd;
  color: black;
}

.fileContainer {
  overflow: hidden;
  position: relative;
  background-color:#c0c7cf !important;
  color:white;
  padding:10px;
}

.fileContainer [type=file] {
  cursor: inherit;
  display: block;
  font-size: 999px;
  filter: alpha(opacity=0);
  min-height: 100%;
  min-width: 100%;
  opacity: 0;
  position: absolute;
  right: 0;
  text-align: right;
  top: 0;

}

</style>
