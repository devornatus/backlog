@include('headerDev')

<style>
@media only screen and (max-width: 500px) {

}

</style>


<section class="page-section portfolio" id="portfolio" style="margin-top:10px;">
  <div class="container">
    <div class="row" style="margin-top:20px; margin-bottom:20px;">
      <div class="col-lg-12" align="left">
        <br>
        <b style="font-size:30pt;">Solicitação: <?php echo $solicitacao_id; ?></b>
        <a href="{{ URL::to('/home_dev') }}"  class="btn btn-primary" type="button" style="float:right;">
          Voltar
        </a>
      </div>
    </div>
    <div class="row">

      <div class="col-lg-12" style="margin-top:20px;">
        <span><b>Sistema:</b></span>
        <?php echo $info_solicitacao[0]->descricao_sistema;?>
      </div>

      <div class="col-lg-4" style="margin-top:20px;">
        <span><b>Data Criação:</b></span>
        <br>
        <?php echo date("d/m/Y", strtotime($info_solicitacao[0]->date_added));?>
      </div>

      <div class="col-lg-4" style="margin-top:20px;">
        <span><b>Data Entrega:</b></span>
        <br>
        <?php if($info_solicitacao[0]->status_id == 1){ ?>
          <input type="date" id="data_entrega" class="form-control" />
        <?php }else{?>
          <?php echo ($info_solicitacao[0]->data_entrega) ? date("d/m/Y", strtotime($info_solicitacao[0]->data_entrega)) : '';?>
        <?php }?>
      </div>

      <div class="col-lg-4" style="margin-top:20px;">
        <span><b>Data Produção:</b></span>
        <br>
        <?php echo ($info_solicitacao[0]->data_producao) ? date("d/m/Y", strtotime($info_solicitacao[0]->data_producao)) : '';?>
      </div>

      <div class="col-lg-4" style="margin-top:20px;">
        <span><b>Status:</b></span>
        <br>
        <?php if( $info_solicitacao[0]->status_id == 1){ ?>

          <select class="form-control" id="status_id" >
            <option value="0">Selecionar</option>
            <?php foreach ($status as $stat) { ?>
              <?php if($stat->status_id == 2 || $stat->status_id == 7){ ?>
                <option value="<?php echo $stat->status_id;?>"><?php echo $stat->descricao;?></option>
              <?php } ?>
            <?php } ?>
          </select>

        <?php }else{?>

          <?php echo $info_solicitacao[0]->descricao_status; ?>

        <?php }?>
      </div>


      <div class="col-lg-4" style="margin-top:20px;">
        <span><b>Desenvolvedor:</b></span>
        <select class="form-control" id="dev_id">
          <option value="0">Selecionar</option>
          <?php foreach ($devs as $dev) { ?>
            <option <?php echo ($info_solicitacao[0]->user_dev_id == $dev->user_id)? 'selected' : '' ;?> value="<?php echo $dev->user_id;?>"><?php echo $dev->username;?></option>
          <?php } ?>
        </select>
      </div>

      <?php if( $info_solicitacao[0]->status_id > 1){ ?>
        <div class="col-lg-4" style="margin-top:20px;">
          <span><b>Branch:</b></span>
          <input class="form-control" id="branch" type="text" value="<?php echo isset($info_solicitacao[0]->branch) ? $info_solicitacao[0]->branch : '';?>" />
        </div>
      <?php } ?>

      <div class="col-lg-12" style="margin-top:20px;">
        <span><b>Qual a solicitação ?</b></span>
        <br>
        <?php echo $info_solicitacao[0]->descricao;?>
      </div>

      <div class="col-lg-12" style="margin-top:20px;">
        <span><b>Para qual finalidade ?</b></span>
        <br>
        <?php echo $info_solicitacao[0]->finalidade;?>
      </div>

      <?php if($info_solicitacao[0]->status_id == 7){ ?>
      <div class="col-lg-12" style="margin-top:20px;">
        <span><b>Justificativa:</b></span>
        <br>
        <?php echo $info_solicitacao[0]->justificativa;?>
      </div>
    <?php } ?>

      <?php if( $info_solicitacao[0]->status_id == 2 && $info_solicitacao[0]->user_dev_id == $_SESSION['usuario']->user_id){ ?>
        <div class="col-lg-12" style="margin-top:20px;">
          <button class="btn btn-warning" style="float:right; color:white;" id="start_develop">Iniciar Desenvolvimento</button>
        </div>
      <?php }elseif( ($info_solicitacao[0]->status_id == 3 || $info_solicitacao[0]->status_id == 8 ) && $info_solicitacao[0]->user_dev_id == $_SESSION['usuario']->user_id){ ?>
        <div class="col-lg-12" style="margin-top:20px;">
          <button class="btn btn-warning" style="float:right; color:white;" id="homologation">Em Homologação</button>
        </div>

      <?php }elseif( $info_solicitacao[0]->status_id == 4 ){ ?>
        <div class="col-lg-12" style="margin-top:20px;">
          <button class="btn btn-danger" style="float:right; color:white; margin:10px;" id="reprove_homologation_solicitation">Reprovar</button>
          <button class="btn btn-success" style="float:right; color:white; margin:10px;" id="approve_waiting_production">Aprovar & Ag. Produção</button>
        </div>
      <?php }elseif( $info_solicitacao[0]->status_id == 5 ){ ?>
        <div class="col-lg-12" style="margin-top:20px;">
          <button class="btn btn-success" style="float:right; color:white; margin:10px;" id="production">Em produção</button>
        </div>
      <?php } ?>

      <?php if( $info_solicitacao[0]->status_id > 1 && $info_solicitacao[0]->user_dev_id == $_SESSION['usuario']->user_id){ ?>

        <div class="col-lg-12" style="margin-top:20px;">
          <span><b>Adicionar SQL:</b></span>
          <textarea id="descricao_sql" name="" rows="4" cols="50" class="form-control"></textarea>
          <br>
          <button class="btn btn-success" style="float:right;" id="add_sql">Adicionar SQL</button>
        </div>
      <?php } ?>

      <div class="col-lg-12" id="div_descricao" style="margin-top:20px;">
        <span><b>Adicionar Mensagem:</b></span>
        <textarea id="descricao" name="" rows="4" cols="50" class="form-control"></textarea>
        <br>

        <?php if($info_solicitacao[0]->status_id == 1){ ?>
          <button class="btn btn-success" id="start_solicitation" style="color:white; float:right; margin-top:23px;">Atribuir Solicitação</button>
        <?php }else{ ?>
          <button class="btn btn-primary" style="float:right;" id="add_comment">Adicionar Mensagem</button>
        <?php } ?>
      </div>

      <div class="col-lg-12 d-none" id="div_finalizar_sem_desenvolvimento"  style="margin-top:20px;">
        <span><b>Adicionar Mensagem:</b></span>
        <textarea id="justificativa" name="" rows="4" cols="50" class="form-control"></textarea>
        <br>
          <button class="btn btn-danger" style="float:right;" id="finish_solicitation">Finalizar Sem Desenvolvimento</button>
      </div>

      <div class="col-lg-12" style="margin-top:20px; ">
        <b><span style="font-size:20pt;">SQL:</span></b>
        <br>
        <div style="width: 100%; height: 300px; overflow-y: scroll;" >
          <table class="table">
            <?php foreach ($sqls as $sql) { ?>
              <tr>
                <td>
                  <b><?php echo $sql->username." - ".date("d/m/Y", strtotime($sql->date_added)); ?>: </b>
                </td>
                <td>
                  <?php echo $sql->texto;?>
                  <button class="btn btn-danger remove_sql" data-id="<?php echo $sql->solicitacao_sql_id;?>" style="float:right;"><i class="fa fa-trash" aria-hidden="true"></i></button>
                </td>
              </tr>
            <?php } ?>
          </table>
        </div>
      </div>
      <br>
      <div class="col-lg-12" style="margin-top:40px; ">
        <b><span style="font-size:20pt;">Mensagens:</span></b>

        <div style="width: 100%; height: 300px; overflow-y: scroll;" >
          <table class="table">
            <?php foreach ($mensagens as $mensagem) { ?>
              <tr>
                <td>
                  <b><?php echo $mensagem->username." - ".date("d/m/Y", strtotime($mensagem->date_added)); ?>: </b>
                </td>
                <td>
                  <?php echo $mensagem->texto;?>
                </td>
              </tr>
            <?php } ?>
          </table>
        </div>
      </div>

      <b><span style="font-size:20pt;">Imagens:</span></b>
        <?php $i = 1; ?>
        <div class="col-lg-12" align="center" style="margin-top:30px; border:3px solid #ced4da; border-radius:5px; padding:20px;">
          <div id="img<?php echo $i; ?>" class="box" >
          </div>
          <label class="fileContainer addFile<?php echo $i; ?>" style="margin:-10px; border-radius: 5px; text-align-last: center;  width:200px; height:200px;"  >
            <i style="font-size:30pt; margin-top:35%" class="fa fa-camera" aria-hidden="true"></i>
            <input name="image" class="image inputFile<?php echo $i; ?>" type="file" accept="image/*" data-id="<?php echo $i; ?>">
            <br>
            <b>Inserir Imagem</b>
          </label>
          <label class="fileContainer deleteFile<?php echo $i; ?> d-none" style=" font-size:10pt; height: 37px !important; margin-top:3px; background-color:#30e34b !important; width:130px; text-align:center !important; border-radius: 20px;" data-id="<?php echo $i; ?>" onclick="saveImage(<?php echo $i; ?>);"><i class="fa fa-check-circle" aria-hidden="true"></i>
          </label>
          <label class="fileContainer deleteFile<?php echo $i; ?> d-none" style=" font-size:10pt; height: 37px !important; margin-top:3px; background-color:red !important; width:130px; text-align:center !important; border-radius: 20px;" data-id="<?php echo $i; ?>" onclick="deleteFile(<?php echo $i; ?>);"><i class="fa fa-trash" aria-hidden="true"></i>
          </label>
          <input class="textimg<?php echo $i; ?> d-none" name="image[]" data-id="<?php echo $i; ?>" type="text" value="" />
        </div>


      <?php if(count($images) > 0 ){ ?>
        <div class="col-lg-12" style="margin-top:20px;">

          <table class="table">

            <tbody>
              <?php foreach ($images as $image) { ?>
                <tr>
                  <td>
                    <?php echo $image->username;?> - <?php echo date("d/m/Y", strtotime($image->date_added));?>
                  </td>
                  <td align="right">
                    <a href="<?php echo "../".$image->local_file.$image->name?>" target="_blank" style="margin-right:40px;">
                      <img class="img-fluid img-thumbnail" style=" max-height: 150px;  max-width: 150px; border-radius:10px;"  src="<?php echo "../".$image->local_file.$image->name?>" alt="" />
                    </a>
                    <button class="btn btn-danger remove_img" data-id="<?php echo $image->image_id?>" style="float:right;"><i class="fa fa-trash" aria-hidden="true"></i></button>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>

        </div>
      <?php } ?>

    </div>

  </div>





</section>



@include('footerDev')

<script>

function deleteFile(id){

  $('.textimg'+id).val('');
  $('.addFile'+id).removeClass('d-none');
  $('.addFile'+id).addClass('show');
  $('#img'+id).html('');
  $('.deleteFile'+id).addClass('d-none');
  $('.deleteFile'+id).removeClass('show');
  $('.inputFile'+id).val('');

}

function saveImage(id){

  var image_id = $('.textimg'+id).val();

  swal({
    title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
    text: 'Aguarde ...',
    html: true,
    showCancelButton: false,
    showConfirmButton: false,
    closeOnConfirm: false,
    closeOnCancel: false
  });

  solicitacao_id = <?php echo $solicitacao_id?>;

  var url = "{{ URL::to('/linkImagebySolicitationId') }}";

  $.ajax({
    url: url,
    method:'POST',
    data: {
      solicitacao_id:solicitacao_id,
      image_id:image_id,
      "_token":"{{ csrf_token() }}"
    },
    success: function(data, textStatus, xhr)
    {

      if(data.response == 'true' || data.response == true)
      {
        swal({
          title: "Sucesso !",
          text: "Imagem Adicionada!",
          type: "success",
          showCancelButton: false,
          confirmButtonText: "Ok",
          cancelButtonText: "Não",
          allowOutsideClick: false,
          closeOnConfirm: false,

        },
        function() {
          location.reload();
        });
      }
      else
      {
        swal("Erro!", data.error_msg, "error");
        $('.remove_img').attr('disabled',false);
        return false;
      }

    },
    error: function(xhr, status, error)
    {
      swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
      $('.remove_img').attr('disabled',false);
    }

  })
  .done(function( data )
  {

  })
  .fail(function( jqXHR ) {
    swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
    $('.remove_img').attr('disabled',false);
  });

}



$('#status_id').change(function (event) {
  status_id = $('#status_id option:selected').val();

  if(status_id == 2){
      $('#div_descricao').removeClass('d-none');
      $('#div_finalizar_sem_desenvolvimento').addClass('d-none');
  }

  if(status_id == 7){
      $('#div_finalizar_sem_desenvolvimento').removeClass('d-none');
      $('#div_descricao').addClass('d-none');
  }

});

$(document).ready(function () {

  $('#start_solicitation').click(function (event) {

    $('#start_solicitation').attr('disabled',true);

    swal({
      title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
      text: 'Aguarde ...',
      html: true,
      showCancelButton: false,
      showConfirmButton: false,
      closeOnConfirm: false,
      closeOnCancel: false
    });

    let today = new Date().toISOString().slice(0, 10)

    solicitacao_id = <?php echo $solicitacao_id?>;
    data_entrega = $('#data_entrega').val();
    descricao = $('#descricao').val();
    status_id = $('#status_id option:selected').val();
    dev_id = $('#dev_id option:selected').val();

    if(data_entrega < today)
    {
      swal("Atenção!", "Data de Entrega precisa ser maior ou igual a hoje!", "");
      $('#start_solicitation').attr('disabled',false);
      return false;
    }

    if(status_id == 0)
    {
      swal("Atenção!", "Selecione um status!", "");
      $('#start_solicitation').attr('disabled',false);
      return false;
    }


    if(dev_id == 0)
    {
      swal("Atenção!", "Selecione um desenvolvedor!", "");
      $('#start_solicitation').attr('disabled',false);
      return false;
    }

    if(descricao.length <= 10)
    {
      swal("Atenção!", "Campo de descrição/mensagem vazio, favor preencher!", "");
      $('#start_solicitation').attr('disabled',false);
      return false;
    }


    if(data_entrega.length <= 5)
    {
      swal("Atenção!", "Selecione uma data de entrega, favor preencher!", "");
      $('#start_solicitation').attr('disabled',false);
      return false;
    }

    var url = "{{ URL::to('/start_solicitation') }}";




    $.ajax({
      url: url,
      method:'POST',
      data: {
        descricao:descricao,
        data_entrega:data_entrega,
        status_id:status_id,
        dev_id:dev_id,
        solicitacao_id:solicitacao_id,
        "_token":"{{ csrf_token() }}"
      },
      success: function(data, textStatus, xhr)
      {

        if(data.response == 'true' || data.response == true)
        {
          swal({
            title: "Sucesso !",
            text: "Solicitação iniciada!",
            type: "success",
            showCancelButton: false,
            confirmButtonText: "Ok",
            cancelButtonText: "Não",
            allowOutsideClick: false,
            closeOnConfirm: false,

          },
          function() {
            location.reload();
          });
        }
        else
        {
          swal("Erro!", data.error_msg, "error");
          $('#start_solicitation').attr('disabled',false);
          return false;
        }

      },
      error: function(xhr, status, error)
      {
        swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
        $('#start_solicitation').attr('disabled',false);
      }

    })
    .done(function( data )
    {

    })
    .fail(function( jqXHR ) {
      swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
      $('#start_solicitation').attr('disabled',false);
    });



  });

  $('#add_comment').click(function (event) {

    $('#add_comment').attr('disabled',true);

    swal({
      title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
      text: 'Aguarde ...',
      html: true,
      showCancelButton: false,
      showConfirmButton: false,
      closeOnConfirm: false,
      closeOnCancel: false
    });

    solicitacao_id = <?php echo $solicitacao_id?>;
    descricao = $('#descricao').val();

    if(descricao.length <= 10){
      swal("Atenção!", "Campo de mensagem vazio, favor preencher!", "");
      $('#add_comment').attr('disabled',false);
      return false;
    }

    var url = "{{ URL::to('/add_comment') }}";

    $.ajax({
      url: url,
      method:'POST',
      data: {
        descricao:descricao,
        solicitacao_id:solicitacao_id,
        "_token":"{{ csrf_token() }}"
      },
      success: function(data, textStatus, xhr)
      {

        if(data.response == 'true' || data.response == true)
        {
          swal({
            title: "Sucesso !",
            text: "Mensagem salva com sucesso!",
            type: "success",
            showCancelButton: false,
            confirmButtonText: "Ok",
            cancelButtonText: "Não",
            allowOutsideClick: false,
            closeOnConfirm: false,

          },
          function() {
            location.reload();
          });
        }
        else
        {
          swal("Erro!", data.error_msg, "error");
          $('#add_comment').attr('disabled',false);
          return false;
        }

      },
      error: function(xhr, status, error)
      {
        swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
        $('#add_comment').attr('disabled',false);
      }

    })
    .done(function( data )
    {

    })
    .fail(function( jqXHR ) {
      swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
      $('#add_comment').attr('disabled',false);
    });



  });


  $('#add_sql').click(function (event) {

    $('#add_sql').attr('disabled',true);

    swal({
      title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
      text: 'Aguarde ...',
      html: true,
      showCancelButton: false,
      showConfirmButton: false,
      closeOnConfirm: false,
      closeOnCancel: false
    });

    solicitacao_id = <?php echo $solicitacao_id?>;
    descricao_sql = $('#descricao_sql').val();

    if(descricao.length <= 10){
      swal("Atenção!", "Campo do SQL vazio, favor preencher!", "");
      $('#start_solicitation').attr('disabled',false);
      return false;
    }

    var url = "{{ URL::to('/add_sql') }}";

    $.ajax({
      url: url,
      method:'POST',
      data: {
        descricao_sql:descricao_sql,
        solicitacao_id:solicitacao_id,
        "_token":"{{ csrf_token() }}"
      },
      success: function(data, textStatus, xhr)
      {

        if(data.response == 'true' || data.response == true)
        {
          swal({
            title: "Sucesso !",
            text: "SQL salvo com sucesso!",
            type: "success",
            showCancelButton: false,
            confirmButtonText: "Ok",
            cancelButtonText: "Não",
            allowOutsideClick: false,
            closeOnConfirm: false,

          },
          function() {
            location.reload();
          });
        }
        else
        {
          swal("Erro!", data.error_msg, "error");
          $('#add_sql').attr('disabled',false);
          return false;
        }

      },
      error: function(xhr, status, error)
      {
        swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
        $('#add_sql').attr('disabled',false);
      }

    })
    .done(function( data )
    {

    })
    .fail(function( jqXHR ) {
      swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
      $('#add_sql').attr('disabled',false);
    });



  });

  $('.remove_sql').click(function (event) {

    $('.remove_sql').attr('disabled',true);

    swal({
      title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
      text: 'Aguarde ...',
      html: true,
      showCancelButton: false,
      showConfirmButton: false,
      closeOnConfirm: false,
      closeOnCancel: false
    });

    solicitacao_id = <?php echo $solicitacao_id?>;
    solicitacao_sql_id = $(this).data('id');

    var url = "{{ URL::to('/remove_sql') }}";

    $.ajax({
      url: url,
      method:'POST',
      data: {
        solicitacao_id:solicitacao_id,
        solicitacao_sql_id:solicitacao_sql_id,
        "_token":"{{ csrf_token() }}"
      },
      success: function(data, textStatus, xhr)
      {

        if(data.response == 'true' || data.response == true)
        {
          swal({
            title: "Sucesso !",
            text: "SQL removido!",
            type: "success",
            showCancelButton: false,
            confirmButtonText: "Ok",
            cancelButtonText: "Não",
            allowOutsideClick: false,
            closeOnConfirm: false,

          },
          function() {
            location.reload();
          });
        }
        else
        {
          swal("Erro!", data.error_msg, "error");
          $('.remove_sql').attr('disabled',false);
          return false;
        }

      },
      error: function(xhr, status, error)
      {
        swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
        $('.remove_sql').attr('disabled',false);
      }

    })
    .done(function( data )
    {

    })
    .fail(function( jqXHR ) {
      swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
      $('.remove_sql').attr('disabled',false);
    });



  });

  $('#start_develop').click(function (event) {

    $('#start_develop').attr('disabled',true);

    swal({
      title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
      text: 'Aguarde ...',
      html: true,
      showCancelButton: false,
      showConfirmButton: false,
      closeOnConfirm: false,
      closeOnCancel: false
    });

    solicitacao_id = <?php echo $solicitacao_id?>;
    branch = $('#branch').val();
    dev_id = $('#dev_id option:selected').val();

    if(dev_id == 0){
      swal("Atenção!", "Selecione um desenvolvedor!", "");
      $('#start_develop').attr('disabled',false);
      return false;
    }

    if(branch.length <= 5){
      swal("Atenção!", "Campo branch vazio, favor preencher!", "");
      $('#start_develop').attr('disabled',false);
      return false;
    }

    var url = "{{ URL::to('/start_develop') }}";

    $.ajax({
      url: url,
      method:'POST',
      data: {
        branch:branch,
        dev_id:dev_id,
        solicitacao_id:solicitacao_id,
        "_token":"{{ csrf_token() }}"
      },
      success: function(data, textStatus, xhr)
      {

        if(data.response == 'true' || data.response == true)
        {
          swal({
            title: "Sucesso !",
            text: "Solicitação em desenvolvimento!",
            type: "success",
            showCancelButton: false,
            confirmButtonText: "Ok",
            cancelButtonText: "Não",
            allowOutsideClick: false,
            closeOnConfirm: false,

          },
          function() {
            location.reload();
          });
        }
        else
        {
          swal("Erro!", data.error_msg, "error");
          $('#start_develop').attr('disabled',false);
          return false;
        }

      },
      error: function(xhr, status, error)
      {
        swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
        $('#start_develop').attr('disabled',false);
      }

    })
    .done(function( data )
    {

    })
    .fail(function( jqXHR ) {
      swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
      $('#start_develop').attr('disabled',false);
    });



  });

  $('#homologation').click(function (event) {

    $('#homologation').attr('disabled',true);

    swal({
      title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
      text: 'Aguarde ...',
      html: true,
      showCancelButton: false,
      showConfirmButton: false,
      closeOnConfirm: false,
      closeOnCancel: false
    });

    solicitacao_id = <?php echo $solicitacao_id?>;

    var url = "{{ URL::to('/homologation') }}";

    $.ajax({
      url: url,
      method:'POST',
      data: {
        solicitacao_id:solicitacao_id,
        "_token":"{{ csrf_token() }}"
      },
      success: function(data, textStatus, xhr)
      {

        if(data.response == 'true' || data.response == true)
        {
          swal({
            title: "Sucesso !",
            text: "Solicitação em homologação!",
            type: "success",
            showCancelButton: false,
            confirmButtonText: "Ok",
            cancelButtonText: "Não",
            allowOutsideClick: false,
            closeOnConfirm: false,

          },
          function() {
            location.reload();
          });
        }
        else
        {
          swal("Erro!", data.error_msg, "error");
          $('#homologation').attr('disabled',false);
          return false;
        }

      },
      error: function(xhr, status, error)
      {
        swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
        $('#homologation').attr('disabled',false);
      }

    })
    .done(function( data )
    {

    })
    .fail(function( jqXHR ) {
      swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
      $('#homologation').attr('disabled',false);
    });



  });

  $("#reprove_homologation_solicitation").click(function(){

    $('#reprove_homologation_solicitation').attr("disabled", true);

    solicitacao_id = <?php echo $solicitacao_id?>;

    swal({
      title: "Tem certeza?",
      text: "Deseja enviar para a correção esta solicitação?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "green",
      confirmButtonText: "Sim",
      cancelButtonText: "Não",
      allowOutsideClick: false,
      closeOnConfirm: false
    },
    function(isconfirm) {
      if(isconfirm){

        swal({
          title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
          text: 'Aguarde ...',
          html: true,
          showCancelButton: false,
          showConfirmButton: false,
          closeOnConfirm: false,
          closeOnCancel: false
        });

        url = "{{ URL::to('/reprove_homologation_solicitation/') }}";

        $.ajax({
          url: url,
          method:'POST',
          data: {
            solicitacao_id:solicitacao_id,
            "_token":"{{ csrf_token() }}"
          },
          success: function(data, textStatus, xhr)
          {

            if(data.response == 'true' || data.response == true)
            {
              swal({
                title: "Sucesso !",
                text: "Solicitação enviado para correção!",
                type: "success",
                showCancelButton: false,
                confirmButtonText: "Ok",
                cancelButtonText: "Não",
                allowOutsideClick: false,
                closeOnConfirm: false,

              },
              function() {
                location.reload();
              });
            }
            else
            {
              swal("Erro!", data.error_msg, "error");
              $('#reprove_homologation_solicitation').attr('disabled',false);
              return false;
            }

          },
          error: function(xhr, status, error)
          {
            swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
            $('#reprove_homologation_solicitation').attr('disabled',false);
          }

        })
        .done(function( data )
        {

        })
        .fail(function( jqXHR ) {
          swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
          $('#reprove_homologation_solicitation').attr('disabled',false);
        });


      }else{
        $('#reprove_homologation_solicitation').attr("disabled", false);
      }

    });

  });

  $("#approve_waiting_production").click(function(){

    $('#approve_waiting_production').attr("disabled", true);

    solicitacao_id = <?php echo $solicitacao_id?>;

    swal({
      title: "Tem certeza?",
      text: "Deseja aprovar esta solicitação?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "green",
      confirmButtonText: "Sim",
      cancelButtonText: "Não",
      allowOutsideClick: false,
      closeOnConfirm: false
    },
    function(isconfirm) {
      if(isconfirm){

        swal({
          title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
          text: 'Aguarde ...',
          html: true,
          showCancelButton: false,
          showConfirmButton: false,
          closeOnConfirm: false,
          closeOnCancel: false
        });

        url = "{{ URL::to('/approve_waiting_production/') }}";

        $.ajax({
          url: url,
          method:'POST',
          data: {
            solicitacao_id:solicitacao_id,
            "_token":"{{ csrf_token() }}"
          },
          success: function(data, textStatus, xhr)
          {

            if(data.response == 'true' || data.response == true)
            {
              swal({
                title: "Sucesso !",
                text: "Solicitação aprovada!",
                type: "success",
                showCancelButton: false,
                confirmButtonText: "Ok",
                cancelButtonText: "Não",
                allowOutsideClick: false,
                closeOnConfirm: false,

              },
              function() {
                location.reload();
              });
            }
            else
            {
              swal("Erro!", data.error_msg, "error");
              $('#approve_waiting_production').attr('disabled',false);
              return false;
            }

          },
          error: function(xhr, status, error)
          {
            swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
            $('#approve_waiting_production').attr('disabled',false);
          }

        })
        .done(function( data )
        {

        })
        .fail(function( jqXHR ) {
          swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
          $('#approve_waiting_production').attr('disabled',false);
        });


      }else{
        $('#approve_waiting_production').attr("disabled", false);
      }

    });

  });


  $('#production').click(function (event) {

    $('#production').attr('disabled',true);

    swal({
      title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
      text: 'Aguarde ...',
      html: true,
      showCancelButton: false,
      showConfirmButton: false,
      closeOnConfirm: false,
      closeOnCancel: false
    });

    solicitacao_id = <?php echo $solicitacao_id?>;

    var url = "{{ URL::to('/production') }}";

    $.ajax({
      url: url,
      method:'POST',
      data: {
        solicitacao_id:solicitacao_id,
        "_token":"{{ csrf_token() }}"
      },
      success: function(data, textStatus, xhr)
      {

        if(data.response == 'true' || data.response == true)
        {
          swal({
            title: "Sucesso !",
            text: "Solicitação em produção!",
            type: "success",
            showCancelButton: false,
            confirmButtonText: "Ok",
            cancelButtonText: "Não",
            allowOutsideClick: false,
            closeOnConfirm: false,

          },
          function() {
            location.reload();
          });
        }
        else
        {
          swal("Erro!", data.error_msg, "error");
          $('#production').attr('disabled',false);
          return false;
        }

      },
      error: function(xhr, status, error)
      {
        swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
        $('#production').attr('disabled',false);
      }

    })
    .done(function( data )
    {

    })
    .fail(function( jqXHR ) {
      swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
      $('#production').attr('disabled',false);
    });



  });


  $('#finish_solicitation').click(function (event) {

    $('#finish_solicitation').attr('disabled',true);

    swal({
      title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
      text: 'Aguarde ...',
      html: true,
      showCancelButton: false,
      showConfirmButton: false,
      closeOnConfirm: false,
      closeOnCancel: false
    });

    solicitacao_id = <?php echo $solicitacao_id?>;
    justificativa = $('#justificativa').val();


    if(justificativa.length <= 5){
      swal("Atenção!", "Campo justificativa vazio, favor preencher!", "");
      $('#finish_solicitation').attr('disabled',false);
      return false;
    }

    var url = "{{ URL::to('/finish_solicitation') }}";

    $.ajax({
      url: url,
      method:'POST',
      data: {
        solicitacao_id:solicitacao_id,
        justificativa:justificativa,

        "_token":"{{ csrf_token() }}"
      },
      success: function(data, textStatus, xhr)
      {

        if(data.response == 'true' || data.response == true)
        {
          swal({
            title: "Sucesso !",
            text: "Solicitação finalizada sem desenvolvimento!",
            type: "success",
            showCancelButton: false,
            confirmButtonText: "Ok",
            cancelButtonText: "Não",
            allowOutsideClick: false,
            closeOnConfirm: false,

          },
          function() {
            location.reload();
          });
        }
        else
        {
          swal("Erro!", data.error_msg, "error");
          $('#finish_solicitation').attr('disabled',false);
          return false;
        }

      },
      error: function(xhr, status, error)
      {
        swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
        $('#finish_solicitation').attr('disabled',false);
      }

    })
    .done(function( data )
    {

    })
    .fail(function( jqXHR ) {
      swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
      $('#finish_solicitation').attr('disabled',false);
    });



  });


  $('.image').change(function (event) {

    swal({
      title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
      text: 'Aguarde ...',
      html: true,
      showCancelButton: false,
      showConfirmButton: false,
      closeOnConfirm: false,
      closeOnCancel: false
    });

    var btn = $(this);
    var id_button_file = $(this).data('id');

    $("#base_image").val('');
    form = new FormData();

    form.append('image', event.target.files[0]);
    form.append('_token',"{{ csrf_token() }}");
    form.append('solicitacao_id',<?php echo $info_solicitacao[0]->solicitacao_id;?>);

    var url = "{{ URL::to('/uploadPhoto/') }}";

    $.ajax({
      url: url,
      cache:false,
      contentType:false,
      processData: false,
      data: form,
      type: 'POST',
      success: function (data) {

        if(data.result == 'false' || data.result == false){
          swal("Atenção", "Enviar arquivos somente de até 3Mbs.", "info");
          return false;
        }

        if(data.result == 'diff_file' ){
          swal("Atenção", "Enviar arquivos devem conter os formatos: PNG, JPG, JPEG e GIF.", "info");
          return false;
        }

        var obj = data;

        $('#img'+ id_button_file).html(' <img style="max-height: 200px;  max-width: 200px;" class="img_new img-fluid"  src=../'+ data.image[0].local_file + '' + data.image[0].name + ' ></img>' );

        $('.textimg'+id_button_file).val(data.image[0].image_id);
        $('.addFile'+id_button_file).addClass('d-none');
        $('.addFile'+id_button_file).removeClass('show');
        $('.deleteFile'+id_button_file).removeClass('d-none');
        $('.deleteFile'+id_button_file).addClass('show');

        swal.close();
      }
    });

  });


  $('.remove_img').click(function (event) {

    $('.remove_img').attr('disabled',true);

    swal({
      title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
      text: 'Aguarde ...',
      html: true,
      showCancelButton: false,
      showConfirmButton: false,
      closeOnConfirm: false,
      closeOnCancel: false
    });

    solicitacao_id = <?php echo $solicitacao_id?>;
    image_id = $(this).data('id');

    var url = "{{ URL::to('/remove_img') }}";

    $.ajax({
      url: url,
      method:'POST',
      data: {
        solicitacao_id:solicitacao_id,
        image_id:image_id,
        "_token":"{{ csrf_token() }}"
      },
      success: function(data, textStatus, xhr)
      {

        if(data.response == 'true' || data.response == true)
        {
          swal({
            title: "Sucesso !",
            text: "Imagem removida!",
            type: "success",
            showCancelButton: false,
            confirmButtonText: "Ok",
            cancelButtonText: "Não",
            allowOutsideClick: false,
            closeOnConfirm: false,

          },
          function() {
            location.reload();
          });
        }
        else
        {
          swal("Erro!", data.error_msg, "error");
          $('.remove_img').attr('disabled',false);
          return false;
        }

      },
      error: function(xhr, status, error)
      {
        swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
        $('.remove_img').attr('disabled',false);
      }

    })
    .done(function( data )
    {

    })
    .fail(function( jqXHR ) {
      swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
      $('.remove_img').attr('disabled',false);
    });



  });


})


</script>


<style>

.navbar2 {
  overflow: hidden;
  background-color: #333;
  position: fixed;
  bottom: 0;
  padding:10px;
  width: 100%;
}

.navbar2 a:hover {
  background: #ddd;
  color: black;
}

.fileContainer {
  overflow: hidden;
  position: relative;
  background-color:#c0c7cf !important;
  color:white;
  padding:10px;
}

.fileContainer [type=file] {
  cursor: inherit;
  display: block;
  font-size: 999px;
  filter: alpha(opacity=0);
  min-height: 100%;
  min-width: 100%;
  opacity: 0;
  position: absolute;
  right: 0;
  text-align: right;
  top: 0;

}

</style>
