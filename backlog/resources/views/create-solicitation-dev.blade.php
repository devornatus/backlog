@include('headerDev')

<style>
@media only screen and (max-width: 500px) {

}

</style>



<section class="page-section portfolio" id="portfolio" style="margin-top:10px;">
  <div class="container">
    <div class="row" style="margin-top:20px; margin-bottom:20px;">
      <div class="col-lg-12" align="center">
        <br>
        <h2>Criar Solicitação</h2>
        <a href="{{ URL::to('/home_dev') }}"  class="btn btn-primary" type="button" style="float:right;">
          Voltar
        </a>
      </div>
    </div>
    <div class="row" id="div_posts" style="margin-top:30px;">

      <div class="col-lg-12" style="margin-top:20px;">
        <span><b>Selecionar Usuário:</b></span>
        <select class="form-control" id="user_id">
          <option value="0">Selecionar</option>
          <?php foreach($users as $user){ ?>
            <option value="<?php echo $user->user_id; ?>"><?php echo $user->username; ?></option>
          <?php } ?>
        </select>
      </div>

      <div class="col-lg-12" style="margin-top:20px;">
        <span><b>Informe o Sistema:</b></span>
        <select class="form-control" id="sistema_id">
          <option value="0">Selecionar</option>
          <?php foreach($sistemas as $sistema){ ?>
            <option value="<?php echo $sistema->sistema_id; ?>"><?php echo $sistema->descricao; ?></option>
          <?php } ?>
        </select>
      </div>

      <div class="col-lg-12" style="margin-top:20px;">
        <span><b>Qual a solicitação ?</b></span>
        <textarea id="descricao" name="" rows="4" cols="50" class="form-control"></textarea>
        <div id="textarea_feedback"></div>
      </div>

      <div class="col-lg-12" style="margin-top:20px;">
        <span><b>Para qual finalidade ?</b></span>
        <textarea id="finalidade" name="" rows="4" cols="50" class="form-control"></textarea>
        <div id="textarea_finalidade"></div>
      </div>

      <?php for($i = 1; $i <= 3; $i++){ ?>
        <div class="col-lg-12" align="center" style="margin-top:30px; border:3px solid #ced4da; border-radius:5px; padding:20px;">
          <div id="img<?php echo $i; ?>" class="box" >
          </div>
          <label class="fileContainer addFile<?php echo $i; ?>" style="margin:-10px; border-radius: 5px; text-align-last: center;  width:200px; height:200px;"  >
            <i style="font-size:30pt; margin-top:35%" class="fa fa-camera" aria-hidden="true"></i>
            <input name="image" class="image inputFile<?php echo $i; ?>" type="file" accept="image/*" data-id="<?php echo $i; ?>">
            <br>
            <b>Inserir Imagem</b>
          </label>
          <label class="fileContainer deleteFile<?php echo $i; ?> d-none" style=" font-size:10pt; height: 37px !important; margin-top:3px; background-color:red !important; width:130px; text-align:center !important; border-radius: 20px;" data-id="<?php echo $i; ?>" onclick="deleteFile(<?php echo $i; ?>);">Deletar
          </label>
          <input class="textimg<?php echo $i; ?> d-none" name="image[]" data-id="<?php echo $i; ?>" type="text" value="" />
        </div>

      <?php } ?>

      <div class="col-lg-12" style="margin-top:20px;">
        <button type="button" class="btn btn-primary btn-lg btn-block" id="register">Registrar</button>
      </div>

      <div class="col-lg-12" style="margin-top:20px;">
        <a href="{{ URL::to('/home_dev') }}"  class="btn btn-danger btn-lg btn-block" type="button" >
          Cancelar
        </a>
      </div>

    </div>

  </div>





</section>



@include('footerDev')

<script>

function deleteFile(id){

  $('.textimg'+id).val('');
  $('.addFile'+id).removeClass('d-none');
  $('.addFile'+id).addClass('show');
  $('#img'+id).html('');
  $('.deleteFile'+id).addClass('d-none');
  $('.deleteFile'+id).removeClass('show');
  $('.inputFile'+id).val('');

}

$(document).ready(function () {

  $('#register').click(function (event) {

    $('#register').attr('disabled',true);

    swal({
      title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
      text: 'Aguarde ...',
      html: true,
      showCancelButton: false,
      showConfirmButton: false,
      closeOnConfirm: false,
      closeOnCancel: false
    });

    //imagens
    var arquivos = [];
    var lista_arquivos = document.getElementsByName("image[]");

    for(var i =0; i< lista_arquivos.length;i++){
      arquivos.push(lista_arquivos[i].value);
    }

    descricao = $('#descricao').val();
    finalidade = $('#finalidade').val();
    sistema_id = $('#sistema_id option:selected').val();
    user_id = $('#user_id option:selected').val();


    if(user_id == 0)
    {
      swal("Atenção!", "Selecione um usuário!", "");
      $('#register').attr('disabled',false);
      return false;
    }


    if(sistema_id == 0)
    {
      swal("Atenção!", "Selecione um sistema!", "");
      $('#register').attr('disabled',false);
      return false;
    }

    if(descricao.length <= 10)
    {
      swal("Atenção!", "Campo de descrição vazio, favor preencher!", "");
      $('#register').attr('disabled',false);
      return false;
    }


    if(finalidade.length <= 10)
    {
      swal("Atenção!", "Campo de finalidade vazio, favor preencher!", "");
      $('#register').attr('disabled',false);
      return false;
    }


    if(descricao.length > 200)
    {
      swal("Atenção!", "Campo de descrição deve conter no máximo 200 caracteres!", "");
      $('#register').attr('disabled',false);
      return false;
    }


    if(finalidade.length > 200)
    {
      swal("Atenção!", "Campo de finalidade deve conter no máximo 200 caracteres!", "");
      $('#register').attr('disabled',false);
      return false;
    }

    var url = "{{ URL::to('/createSolicitationDev') }}";

    $.ajax({
      url: url,
      method:'POST',
      data: {
        descricao:descricao,
        finalidade:finalidade,
        sistema_id:sistema_id,
        user_id:user_id,
        arquivos:arquivos,
        "_token":"{{ csrf_token() }}"
      },
      success: function(data, textStatus, xhr)
      {

        if(data.response == 'true' || data.response == true)
        {
          swal({
            title: "Sucesso !",
            text: "Solicitação criada com sucesso!",
            type: "success",
            showCancelButton: false,
            confirmButtonText: "Sim",
            cancelButtonText: "Não",
            allowOutsideClick: false,
            closeOnConfirm: false,

          },
          function() {

            window.location.href = "{{ URL::to('/view_solicitation') }}/"+data.solicitacao_id;
          });
        }
        else
        {
          swal("Erro!", data.error_msg, "error");
          $('#register').attr('disabled',false);
          return false;
        }

      },
      error: function(xhr, status, error)
      {
        swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
        $('#register').attr('disabled',false);
      }

    })
    .done(function( data )
    {

    })
    .fail(function( jqXHR ) {
      swal("Erro!", "Algo de inesperado ocorreu, tente novamente", "error");
      $('#register').attr('disabled',false);
    });



  });

  var text_max = 200;
  $('#textarea_feedback').html(text_max + ' caracteres restantes');

  $('#descricao').keyup(function() {
    var text_length = $('#descricao').val().length;
    var text_remaining = text_max - text_length;

    $('#textarea_feedback').html(text_remaining + ' caracteres restantes');
  });

  $('#textarea_finalidade').html(text_max + ' caracteres restantes');

  $('#finalidade').keyup(function() {
    var text_length = $('#finalidade').val().length;
    var text_remaining = text_max - text_length;

    $('#textarea_finalidade').html(text_remaining + ' caracteres restantes');
  });


    $('.image').change(function (event) {

      swal({
        title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
        text: 'Aguarde ...',
        html: true,
        showCancelButton: false,
        showConfirmButton: false,
        closeOnConfirm: false,
        closeOnCancel: false
      });

      var btn = $(this);
      var id_button_file = $(this).data('id');

      $("#base_image").val('');
      form = new FormData();

      form.append('image', event.target.files[0]);
      form.append('_token',"{{ csrf_token() }}");

      var url = "{{ URL::to('/uploadPhoto/') }}";

      $.ajax({
        url: url,
        cache:false,
        contentType:false,
        processData: false,
        data: form,
        type: 'POST',
        success: function (data) {

          if(data.result == 'false' || data.result == false){
            swal("Atenção", "Enviar arquivos somente de até 3Mbs.", "info");
            return false;
          }

          if(data.result == 'diff_file' ){
            swal("Atenção", "Enviar arquivos devem conter os formatos: PNG, JPG, JPEG e GIF.", "info");
            return false;
          }

          var obj = data;

          $('#img'+ id_button_file).html(' <img style=" max-height: 200px;  max-width: 200px;" class="img_new img-fluid"  src='+ data.image[0].local_file + '' + data.image[0].name + ' ></img>' );

          $('.textimg'+id_button_file).val(data.image[0].image_id);
          $('.addFile'+id_button_file).addClass('d-none');
          $('.addFile'+id_button_file).removeClass('show');
          $('.deleteFile'+id_button_file).removeClass('d-none');
          $('.deleteFile'+id_button_file).addClass('show');

          swal.close();
        }
      });

    });


})


</script>


<style>

.navbar2 {
  overflow: hidden;
  background-color: #333;
  position: fixed;
  bottom: 0;
  padding:10px;
  width: 100%;
}

.navbar2 a:hover {
  background: #ddd;
  color: black;
}

.fileContainer {
  overflow: hidden;
  position: relative;
  background-color:#c0c7cf !important;
  color:white;
  padding:10px;
}

.fileContainer [type=file] {
  cursor: inherit;
  display: block;
  font-size: 999px;
  filter: alpha(opacity=0);
  min-height: 100%;
  min-width: 100%;
  opacity: 0;
  position: absolute;
  right: 0;
  text-align: right;
  top: 0;

}

</style>
