<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
        return redirect('login');
});


Route::get('/login', 'LoginController@login')->name('login');
Route::post('/autenticar', 'LoginController@autenticar')->name('autenticar');
Route::get('/logout', 'LoginController@logout')->name('logout');
Route::get('/sessao-expirada', 'LoginController@sessionExpired')->name('sessao-expirada');

Route::get('/home', 'BacklogController@home')->middleware('verificalogin')->name('home');
Route::get('/criar-solicitacao', 'BacklogController@pageCreateSolicitation')->middleware('verificalogin')->name('criar-solicitacao');
Route::post('/createSolicitation', 'BacklogController@createSolicitation')->middleware('verificalogin')->name('createSolicitation');
Route::get('/visualizar/{solicitacao_id}', 'BacklogController@viewSolicitation')->middleware('verificalogin')->name('visualizar/{solicitacao_id}');
Route::post('/uploadPhoto', 'BacklogController@uploadPhoto')->middleware('verificalogin')->name('uploadPhoto');
Route::post('/updateSolicitation', 'BacklogController@updateSolicitation')->middleware('verificalogin')->name('updateSolicitation');

Route::get('/login_dev', 'LoginController@loginDev')->name('login_dev');
Route::post('/autenticar_dev', 'LoginController@autenticarDev')->name('autenticar_dev');
Route::get('/logout_dev', 'LoginController@logoutDev')->name('logout_dev');

Route::get('/home_dev', 'BacklogController@homeDev')->middleware('verificalogin')->name('home_dev');
Route::get('/view_solicitation/{solicitacao_id}', 'BacklogController@viewSolicitationDev')->middleware('verificalogin')->name('view_solicitation/{solicitacao_id}');
Route::post('/start_solicitation', 'BacklogController@startSolicitation')->middleware('verificalogin')->name('start_solicitation');
Route::post('/add_comment', 'BacklogController@insertMessageBySolicitationId')->middleware('verificalogin')->name('add_comment');
Route::post('/add_sql', 'BacklogController@insertSqlBySolicitationId')->middleware('verificalogin')->name('add_sql');
Route::post('/remove_sql', 'BacklogController@removeSqlBySolicitationId')->middleware('verificalogin')->name('remove_sql');
Route::post('/start_develop', 'BacklogController@startDevelopBySolicitationId')->middleware('verificalogin')->name('start_develop');
Route::post('/homologation', 'BacklogController@homologationBySolicitationId')->middleware('verificalogin')->name('homologation');
Route::post('/reprove_homologation_solicitation', 'BacklogController@reproveHomologationBySolicitationId')->middleware('verificalogin')->name('reprove_homologation_solicitation');
Route::post('/approve_waiting_production', 'BacklogController@aproveHomologationBySolicitationId')->middleware('verificalogin')->name('approve_waiting_production');
Route::post('/production', 'BacklogController@aproveBySolicitationId')->middleware('verificalogin')->name('production');
Route::post('/finish_solicitation', 'BacklogController@finishBySolicitationId')->middleware('verificalogin')->name('finish_solicitation');
Route::get('/criar-solicitacao-dev', 'BacklogController@pageCreateSolicitationDev')->middleware('verificalogin')->name('criar-solicitacao-dev');
Route::post('/createSolicitationDev', 'BacklogController@createSolicitationDev')->middleware('verificalogin')->name('createSolicitationDev');
Route::post('/linkImagebySolicitationId', 'BacklogController@linkImagebySolicitationId')->middleware('verificalogin')->name('linkImagebySolicitationId');
Route::post('/remove_img', 'BacklogController@removeImage')->middleware('verificalogin')->name('remove_img');


Route::get('/search-descripition', 'BacklogController@searchDecription')->middleware('verificalogin')->name('search-descripition');
Route::get('/search-query', 'BacklogController@searchQuery')->middleware('verificalogin')->name('search-query');
